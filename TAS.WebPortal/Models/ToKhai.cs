﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAS.WebPortal.Models
{
    public class TokhaiPerson
    {
        private bool _isFinished = false;
        public string survey_ID { get; set; }
        public string surveyDate { get; set; }
        public Person person { get; set; }

        public List<Question> get_Question { get; set; }
        public bool isFinished
        {
            get { return _isFinished; }
            set { _isFinished = value; }
        }
        public string captcha { get; set; }
        public string tokenID { get; set; }
    }
    public class ToKhai
    {
        private bool _isFinished = false;
        public string survey_ID { get; set; }
        public string surveyDate { get; set; }
        public Employee employee { get; set; }

        public List<Question> get_Question { get; set; }
        public bool isFinished
        {
            get { return _isFinished; }
            set { _isFinished = value; }
        }
        public string captcha { get; set; }
        public string tokenID { get; set; }

    }
    public class Person
    {
        private string _cmnd = string.Empty;
        private string _email = string.Empty;
        public string PersonID { get; set; }
        public string hoten { get; set; }
        public string namsinh { get; set; }
        public int phai { get; set; }
        public string sonha { get; set; }
        public string thon { get; set; }
        public string cholam { get; set; }
        public string matt { get; set; }
        public string maqu { get; set; }
        public string mapxa { get; set; }
        public string hotenkdau { get; set; }
        public string dienthoai { get; set; }
        public string email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string cmnd
        {
            get { return _cmnd; }
            set { _cmnd = value; }
        }
        public string ngaycap { get; set; }
        public string noicap { get; set; }
        public string quoctich { get; set; }
        public virtual string CustomerID { set; get; }
        public virtual string BranchID { set; get; }
    }
    public class Employee
    {
        private int _groupId = 3;
        private string _cmnd = string.Empty;
        private string _email = string.Empty;
        public string employeeID { get; set; }
        public string hoten { get; set; }
        public string namsinh { get; set; }
        public int phai { get; set; }
        public string sonha { get; set; }
        public string thon { get; set; }
        public string cholam { get; set; }
        public string matt { get; set; }
        public string maqu { get; set; }
        public string mapxa { get; set; }
        public string hotenkdau { get; set; }
        public string dienthoai { get; set; }
        public string email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string cmnd 
        {
            get { return _cmnd; }
            set { _cmnd = value; }
        }
        public string ngaycap { get; set; }
        public string noicap { get; set; }
        public int makp { get; set; }
        public string quoctich { get; set; }

        /// <summary>
        /// 1 : Admin, 2 : Quản trị khoa phòng, 3 : người dùng(Mặc định)
        /// </summary>
        public int GroupID
        {
            get { return _groupId; }
            set { _groupId = value; }
        }
        public virtual string CustomerID { set; get; }
        public virtual string BranchID { set; get; }
    }

    public class Question
    {
        public int questionID { get; set; }
        public string questionText { get; set; }
        public int questionOrder { get; set; }

        public List<Answers> answers { get; set; }
    }

    public class Answers
    {
        private string _answerValue = string.Empty;
        private string _answerOtherText = string.Empty;
        private bool _isActive = true;
        private bool _isDeletes = false;
        private bool _answerRequire = false;
        private int _answerID = 0;
        private int _answerType = 0;
        private int _answerOrder = 0;

        public string answerValue
        {
            get { return _answerValue; }
            set { _answerValue = value; }
        }
        public string answerOtherText
        {
            get { return _answerOtherText; }
            set { _answerOtherText = value; }
        }
        public int answerID
        {
            get { return _answerID; }
            set { _answerID = value; }
        }
        public int questionID { get; set; }
        public int answerType
        {
            get { return _answerType; }
            set { _answerType = value; }
        }
        public string answerText { get; set; }
        public bool answerRequire
        {
            get { return _answerRequire; }
            set { _answerRequire = value; }
        }
        public int answerOrder
        {
            get { return _answerOrder; }
            set { _answerOrder = value; }
        }
        public bool isActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        public bool isDelete
        {
            get { return _isDeletes; }
            set { _isDeletes = value; }
        }
    }
}