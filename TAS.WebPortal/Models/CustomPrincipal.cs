﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using TAS.Core.Shared;
namespace TAS.WebPortal.Models
{
    public class CustomPrincipal : ICustomPrincipal
    {     
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role) { return false; }
        public CustomPrincipal(string username)
        {
            this.Identity = new GenericIdentity(username);
        }
        public string Token_ID { set; get; }
        public string Access_token { set; get; }
        public string Fullname { set; get; }
        public string UserID { set; get; }
        public int GroupID { set; get; }
        public string BranchID { set; get; }
        public string BranchName { set; get; }
    }

    public class CustomPrincipalSerializeModel 
    {
        public string Token_ID { set; get; }
        public string Access_token { set; get; }
        public string Fullname { set; get; }
        public string UserID { set; get; }
        public int GroupID { set; get; }
        public string BranchID { set; get; }
        public string BranchName { set; get; }
    }
}