﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAS.WebPortal.Models
{
    public class Tinh
    {
        public string matt { get; set; }
        public string tentt { get; set; }
    }

    public class Quan
    {
        public string maqu { get; set; }
        public string tenquan { get; set; }
    }

    public class Xa
    {
        public string mapxa { get; set; }
        public string tenpxa { get; set; }
    }

    public class Khoa
    {
        public int ma { get; set; }
        public string ten { get; set; }
    }
}