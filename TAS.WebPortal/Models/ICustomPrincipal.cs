﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace TAS.WebPortal.Models
{
    public interface ICustomPrincipal: IPrincipal
    {
        string Token_ID { set; get; }
        string Access_token { set; get; }
        string Fullname { set; get; }
        string UserID { set; get; }
        int GroupID { set; get; }
        string BranchID { set; get; }
        string BranchName { set; get; }
    }
    
}
