﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAS.WebPortal.Models
{
    public class Message
    {
        public int id { get; set; }
        public string noidung { get; set; }
        public int isactive { get; set; }
    }
}