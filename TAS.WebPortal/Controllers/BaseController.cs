﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAS.Core.Shared;
using TAS.WebPortal.Models;
using TAS.WebPortal.Filters;
using System.Web.Security;
using System.Web.Script.Serialization;

namespace TAS.WebPortal.Controllers
{
    [ApplicationAuthorize]
    public class BaseController : Controller
    {
        public BaseController()
        {
            if (User == null || (User != null && string.IsNullOrEmpty(User.Access_token)))
            {
                RedirectToActionPermanent("Login", "Account");
            }
           
        }
        protected virtual new CustomPrincipal User
        {
            get 
            {
                string cookieName = FormsAuthentication.FormsCookieName;
                 
                HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName]  ;
                if (authCookie != null)
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                    if (!authTicket.Expired)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();

                        CustomPrincipalSerializeModel serializeModel = serializer.Deserialize<CustomPrincipalSerializeModel>(authTicket.UserData);

                        CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);

                        newUser.UserID = serializeModel.UserID;
                        newUser.Fullname = serializeModel.Fullname;
                        newUser.Token_ID = serializeModel.Token_ID;
                        newUser.GroupID = serializeModel.GroupID;
                        newUser.Access_token = serializeModel.Access_token;
                        newUser.BranchID = serializeModel.BranchID;
                        newUser.BranchName = serializeModel.BranchName;
                        return newUser;
                    }
                    else return  null;

                    
                }
                else return null;
               
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    [ApplicationAuthorize]
    public class AdminBaseController : BaseController
    {
        protected string _BranchID;
        protected string _BranchName;
        protected int _GroupID; 
        public AdminBaseController():base()
        {
            if (User == null || (User != null && string.IsNullOrEmpty(User.Access_token)))
            {
                RedirectToActionPermanent("Login", "Account");
            }
            else
            {
                _BranchID = User.BranchID;
                _BranchName = User.BranchName;
                _GroupID = User.GroupID;
                if (User.GroupID != 1)
                {
                    RedirectToActionPermanent("Home", "AccessDenied");
                }
            }
           

        }
    }
}