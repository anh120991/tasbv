﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using TAS.Core.Shared;
using TAS.Core.Shared.nCoV;
using TAS.WebPortal.Helper;
using TAS.WebPortal.Provider;

namespace TAS.WebPortal.Controllers
{
    public class DashboardController : Controller
    {
        private string _token;
        private string _token_id;
        // GET: Dashboard
        //https://www.youtube.com/watch?v=HuniMNaWP6U
        public ActionResult Index()
        {
            var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
            var _employeeId = PublicSecureProvider.Decrypt(tmp);
            
            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
            var employeeInfo = new nCOV(_token,null).GetHR_Employees(_employeeId);

            string strDate = DateTime.Now.ToString("yyyy-MM-dd");

            int tongQuanSo = 0;
            int tongBaoCao = 0;
            int tongChuaBaoCao = 0;
            int tongBatThuong = 0;

            string strData = genListKP(-1, ref tongQuanSo, ref tongBaoCao, ref tongChuaBaoCao, ref tongBatThuong);

            ViewBag.tongQuanSo = tongQuanSo;
            ViewBag.tongBaoCao = tongBaoCao;
            ViewBag.tongChuaBaoCao = tongChuaBaoCao;
            ViewBag.tongBatThuong = tongBatThuong;

            ViewBag.List = strData;
            ViewBag.Makp = Gen_Khoa(-1);
            ViewBag.Date = strDate;
            return View();
        }

        public ActionResult Details(int id, string ngay = "", string thang = "", string nam = "")
        {

            _token = string.Empty;
            string tieude = string.Empty;
            string noidung = string.Empty;
            DataSet ds = new nCOV(_token,null).GetReportDaily_Detail(ngay+"/"+thang +"/"+nam, id);
            if(ds != null && ds.Tables.Count > 0)
            {
                //Gen tiêu đề câu trả lời
                tieude = genTieuDe(ds.Tables[0]);

                if (ds.Tables.Count == 2)
                {
                    //Danh sách người dùng
                    noidung = genNoiDung(ds.Tables[1]);
                }
                else
                {
                    noidung = "<tr> <td colspan=20> <center>Không có dữ liệu</center> </td> </tr>";
                }
            }

            ViewBag.CauHoi = tieude;
            ViewBag.genNoiDung = noidung;

            return View();
        }

        public ActionResult contact()
        {
            return View();
        }

        public string genListKP(int mapk, ref int tongQuanSo, ref int tongBaoCao,ref int chuabaocao, ref int tongBatThuong)
        {
            string strData = string.Empty;
            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
            DataSet ds = new nCOV(_token,null).GetReportDaily("");
            int i = 1;
            string ngay = DateTime.Now.ToString("dd/MM/yyyy");
            if (ds != null && ds.Tables.Count > 0)
            {
                ///Gen dữ liệu
                foreach(DataRow r in ds.Tables[0].Rows)
                {
                    int _tongQuanSo = Convert.ToInt32(r["quanso"]);
                    int _tongBaoCao = Convert.ToInt32(r["soluong"]);
                    int _tongBatThuong = Convert.ToInt32(r["batthuong"]);
                    int _chuabaocao = _tongQuanSo - _tongBaoCao;

                    tongQuanSo += _tongQuanSo;
                    tongBaoCao += _tongBaoCao;
                    tongBatThuong += _tongBatThuong;
                    chuabaocao += _chuabaocao;
                    strData += string.Format("<tr data-makp='{6}' data-ngay='{7}'>" +
                                             "<td><center>{0}</center></td>" +
                                             "<td>{1}</td>" +
                                             "<td><center>{2}</center></td>" +
                                             "<td><center>{3}</center></td>" +
                                             "<td><center>{4}</center></td>" +
                                             "<td><center>{5}</center></td>" +
                                             "<td><center><a href='/Dashboard/Details/{6}/{7}'><i class='fa fa-edit' title='Chi tiết'></i></a></center></td>" +
                                             " </tr>", i, r["tenkp"].ToString(), _tongQuanSo, _tongBaoCao, _chuabaocao, _tongBatThuong, r["makp"].ToString(), ngay);
                    i++;
                }
            }

            return strData;
        }

        public JsonResult getListAjax(int mapk, string ngay)
        {
            string strData = string.Empty;
            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();

            DateTime dtNgay = Convert.ToDateTime(ngay);
            string strNgay = dtNgay.ToString("dd/MM/yyyy");

            DataSet ds = new nCOV(_token,null).GetReportDaily(strNgay, mapk);
            int i = 1;

            int tongQuanSo = 0;
            int tongBaoCao = 0;
            int tongChuaBaoCao = 0;
            int tongBatThuong = 0;

            if (ds != null && ds.Tables.Count > 0)
            {
                ///Gen dữ liệu
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    int _tongQuanSo = Convert.ToInt32(r["quanso"]);
                    int _tongBaoCao = Convert.ToInt32(r["soluong"]);
                    int _tongBatThuong = Convert.ToInt32(r["batthuong"]);
                    int _chuabaocao = _tongQuanSo - _tongBaoCao;

                    tongQuanSo += _tongQuanSo;
                    tongBaoCao += _tongBaoCao;
                    tongBatThuong += _tongBatThuong;
                    tongChuaBaoCao += _chuabaocao;
                    strData += string.Format("<tr data-makp='{6}' data-ngay='{7}'>" +
                                             "<td><center>{0}</center></td>" +
                                             "<td>{1}</td>" +
                                             "<td><center>{2}</center></td>" +
                                             "<td><center>{3}</center></td>" +
                                             "<td><center>{4}</center></td>" +
                                             "<td><center>{5}</center></td>" +
                                             "<td><center><a href='/Dashboard/Details/{6}/{7}'><i class='fa fa-edit' title='Chi tiết'></i></a></center></td>" +
                                             " </tr>", i, r["tenkp"].ToString(), _tongQuanSo, _tongBaoCao, _chuabaocao, _tongBatThuong, r["makp"].ToString(), strNgay);
                    i++;
                }

                return Json(new { content = strData, tongQuanSo = tongQuanSo, tongBaoCao = tongBaoCao, tongChuaBaoCao = tongChuaBaoCao, tongBatThuong = tongBatThuong }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { content = ""}, JsonRequestBehavior.AllowGet);
        }
        
        private string genTieuDe(DataTable dtbData)
        {
            string strData = string.Empty;

            if(dtbData != null && dtbData.Rows.Count > 0)
            {

                strData = "<tr>";
                strData += string.Format("<th> {0} </th>", "STT");
                strData += string.Format("<th> {0} </th>", "KHOA/ BAN");
                strData += string.Format("<th> {0} </th>", "HỌ VÀ TÊN");

                foreach (DataRow r in dtbData.Rows)
                {
                    strData += string.Format("<th> {0} </th>", r["short_text"].ToString().ToUpper());
                }

                strData += "</tr>";
            }

            return strData;
        }

        private string genNoiDung(DataTable dtbData)
        {
            string strData = string.Empty;

            if (dtbData != null && dtbData.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in dtbData.Rows)
                {
                    strData += "<tr>";
                    strData += string.Format("<td> {0} </td>", i);
                    strData += string.Format("<td> {0} </td>", r["tenkp"]);
                    strData += string.Format("<td> {0} </td>", r["hoten"]);
                    for(int j = 5; j < dtbData.Columns.Count; j++)
                    {

                        strData += string.Format("<td> <center> {0} </center> </td>", r[j]);
                    }
                    strData += "</tr>";

                    i++;
                }
            }
            else
            {
                strData = "<tr> <td colspan=20> <center>Không có dữ liệu</center>  </td> </tr>";
            }
            return strData;
        }

        private string Gen_Khoa(int mapk)
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "department");
            List<HR_Department> lstDept = new List<HR_Department>();//= GetKhoaphong();
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                lstDept = new nCOV(_token,null).GetKhoaphong();
                // again in memCacher Cache
                MemoryCacheHelper.Add(_key, lstDept, DateTimeOffset.UtcNow.AddDays(3));
            }
            else
            {
                lstDept = (List<HR_Department>)option_system;
            }
            if (lstDept != null && lstDept.Count > 0)
            {
                #region Administrator
                _html = string.Format("<option value='{0}' selected>{1}</option>", -1, "Toàn viện");
                foreach (HR_Department obj in lstDept)
                {
                    if (obj.Ma == mapk)
                    {
                        _html += string.Format("<option value='{0}' selected>{1}</option>", obj.Ma, obj.Ten);
                    }
                    else
                    {
                        _html += string.Format("<option value='{0}'>{1}</option>", obj.Ma, obj.Ten);
                    }
                }
                #endregion
            }
            // _html = option_system.ToString();
            return _html;
        }
        
    }
}