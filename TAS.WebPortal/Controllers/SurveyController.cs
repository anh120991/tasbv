﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TAS.Core.Entities.MasterData.Administrative;
using TAS.Core.Shared;
using TAS.Core.Shared.nCoV;
using TAS.WebPortal.Helper;
using TAS.WebPortal.Models;
using TAS.WebPortal.Provider;

namespace TAS.WebPortal.Controllers
{

    public class SurveyController : BaseController
    {
        private readonly string URL_MASTER = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["HttpBaseAddress"]);
        private readonly string _Province = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultProvince"]);
        private readonly int _pageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PAGESIZE"]);
        // GET: Survey
        private string _token;
        private string _token_id;
        private string _employeeId;
        public SurveyController()
        {
            if (User != null && !string.IsNullOrEmpty(User.Access_token))
            {
                _token = User.Access_token;
                _token_id = User.Token_ID;
                _employeeId = User.UserID;
            }
        }
        //[HttpGet]
        //public ActionResult Index()
        //{
        //    if (System.Web.HttpContext.Current.Session["expires"] != null)
        //    {
        //        #region Check session
        //        /*Kiểm tra token còn hạn ko?*/
        //        string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
        //        if (!string.IsNullOrEmpty(_date))
        //        {
        //            DateTime date = Convert.ToDateTime(_date);
        //            if (date < DateTime.Now)
        //            {
        //                return RedirectToAction("Login", "Account");
        //            }
        //            else
        //            {
        //                var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
        //                _employeeId = PublicSecureProvider.Decrypt(tmp);
        //                //
        //                _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
        //                _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
        //                List<HR_Survey_Answer> lstAnswerFinish = Get_SurveyAnswer();
        //                return View (lstAnswerFinish);
        //            }
        //        }
        //        else
        //        {
        //            return RedirectToAction("Login", "Account");
        //        }
        //        #endregion
        //    }
        //    else return RedirectToAction("Login", "Account");
        //}

        [HttpGet]
        public ActionResult Index()
        {
            int size = _pageSize;
            List<HR_Survey_Answer> lstAnswerFinish = Get_SurveyAnswer();
            //lstAnswerFinish = taoList();
            if (lstAnswerFinish == null)
            {
                return View();
            }
            else
            {
                if (lstAnswerFinish.Count % size != 0)
                {
                    ViewBag.pageNumber = lstAnswerFinish.Count / size + 1;
                }
                else
                {
                    ViewBag.pageNumber = lstAnswerFinish.Count / size;
                }

                if (lstAnswerFinish.Count > _pageSize)
                {
                    //Chỉ lấy đủ size
                    List<HR_Survey_Answer> lstData = new List<HR_Survey_Answer>();
                    for (int i = 0; i < _pageSize; i++)
                    {
                        HR_Survey_Answer obj = new HR_Survey_Answer()
                        {
                            Survey_ID = lstAnswerFinish[i].Survey_ID,
                            EmployeeID = lstAnswerFinish[i].EmployeeID,
                            EffDate = lstAnswerFinish[i].EffDate,
                            EffDate_New = lstAnswerFinish[i].EffDate_New,
                            Token_ID = lstAnswerFinish[i].Token_ID
                        };

                        lstData.Add(obj);
                    }
                    return View(lstData);
                }
                else
                {
                    return View(lstAnswerFinish);
                }
            }

        }

        public string ViewPage(int index)
        {
            int n = _pageSize;
            List<HR_Survey_Answer> lstAnswerFinish = Get_SurveyAnswer();
            //lstAnswerFinish = taoList();
            string strData = string.Empty;
            strData += "<tr style='background:#228154'><th style='color:#fff'> NGÀY </th> <th style='color:#fff'>Chi tiết</th> </tr>";
            int i = n * index;
            int max = i + n;
            if (max > lstAnswerFinish.Count)
                max = lstAnswerFinish.Count;
            for (; i < max; i++)
            {
                strData += "<div class='row'><tr>";
                strData += string.Format("<td>{0}</td><td><a href='/khao-sat/Details/{1}'>Xem chi tiết</a></td>", lstAnswerFinish[i].EffDate_New, lstAnswerFinish[i].Survey_ID);
                strData += "</tr></div >";
            }
            return strData;
        }

        private List<HR_Survey_Answer> taoList()
        {
            List<HR_Survey_Answer> lstData = new List<HR_Survey_Answer>();
            int size = 55;
            for (int i = 0; i < size; i++)
            {
                HR_Survey_Answer obj = new HR_Survey_Answer()
                {
                    Survey_ID = i,
                    EmployeeID = i.ToString(),
                    EffDate = DateTime.Now.AddDays(-i),
                    EffDate_New = DateTime.Now.ToString("dd/MM/yyyy"),
                };
                lstData.Add(obj);
            }
            return lstData;
        }

        public ActionResult Create()
        {
            if (System.Web.HttpContext.Current.Session["expires"] != null)
            {
                #region Check session
                /*Kiểm tra token còn hạn ko?*/
                string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                if (!string.IsNullOrEmpty(_date))
                {
                    DateTime date = Convert.ToDateTime(_date);
                    if (date < DateTime.Now)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
                        _employeeId = PublicSecureProvider.Decrypt(tmp);
                        //
                        _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
                        _token = System.Web.HttpContext.Current.Session["access_token"].ToString();


                        ViewData["EFFDATE"] = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                        string strData1 = string.Empty;
                        string strData2 = string.Empty;
                        var objSurveyForm = Get_SurveyForm();
                        //
                        if (!string.IsNullOrEmpty(objSurveyForm.Employee.Namsinh))
                        {
                            ViewBag.NamSinh = genNamSinh(int.Parse(objSurveyForm.Employee.Namsinh));
                        }
                        else
                        {
                            ViewBag.NamSinh = genNamSinh(null);
                        }


                        ViewBag.Phai = genPhai(objSurveyForm.Employee.Phai);
                        ViewBag.QuocTich = genQuocTich(objSurveyForm.Employee.Quoctich.Trim());
                        ViewBag.KhoaPhong = Gen_Khoa(objSurveyForm.Employee.Makp);
                        ViewBag.Tinh = genTinh(objSurveyForm.Employee.Matt);
                        ViewBag.Quan = genQuan(objSurveyForm.Employee.Maqu, objSurveyForm.Employee.Matt);
                        ViewBag.Xa = genXa(objSurveyForm.Employee.Mapxa, objSurveyForm.Employee.Maqu);
                        int count = genCauHoi(objSurveyForm.Get_Question, ref strData1, ref strData2, objSurveyForm.IsFinished);
                        ViewBag.CauHoiChon = strData1;
                        ViewBag.CauHoi = strData2;
                        //
                        return View(objSurveyForm);

                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
                #endregion
            }
            else return RedirectToAction("Login", "Account");
        }

        // POST: Employee/Create
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult Create(string objToKhai)
        {
            bool isUpdate = false;
            if (System.Web.HttpContext.Current.Session["expires"] != null)
            {
                #region Check session
                /*Kiểm tra token còn hạn ko?*/
                string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                if (!string.IsNullOrEmpty(_date))
                {
                    DateTime date = Convert.ToDateTime(_date);
                    if (date < DateTime.Now)
                    {
                        return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
                        _employeeId = PublicSecureProvider.Decrypt(tmp);
                        //
                        _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
                        _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                        try
                        {
                            ToKhai obj = new ToKhai();
                            obj = JsonConvert.DeserializeObject<ToKhai>(objToKhai);
                            obj.survey_ID = "-1";
                            obj.surveyDate = DateTime.Now.ToString("dd/MM/yyyy");
                            obj.tokenID = System.Web.HttpContext.Current.Session["token_id"].ToString();

                            isUpdate = new nCOV(_token, null).Save(obj);
                        }
                        catch (Exception objEx)
                        {
                            return Json(new { content = objEx, iserror = false }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new { content = "Cập nhật thành công!", iserror = isUpdate }, JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {
                    return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);
                }
                #endregion
            }
            else return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);

            //try
            //{
            //    // TODO: Add insert logic here
            //    int i=collection.Count;
            //    return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}

        }

        // GET: Employee/Details/5
        public ActionResult Details(int? id)
        {
            if (!id.HasValue) return RedirectToAction("Index", "Home");
            if (System.Web.HttpContext.Current.Session["expires"] != null)
            {
                #region Check session
                /*Kiểm tra token còn hạn ko?*/
                string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                if (!string.IsNullOrEmpty(_date))
                {
                    DateTime date = Convert.ToDateTime(_date);
                    if (date < DateTime.Now)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
                        _employeeId = PublicSecureProvider.Decrypt(tmp);
                        //
                        _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
                        _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                        SurveyModelForm_Extend objView = Get_SurveyFormDetail(id.Value);
                        ViewData["EFFDATE"] = objView.SurveyDate;

                        string strData1 = string.Empty;
                        string strData2 = string.Empty;

                        int count = genCauHoi(objView.Get_Question, ref strData1, ref strData2, true);
                        ViewBag.CauHoiChon = strData1;
                        ViewBag.CauHoi = strData2;

                        return View(objView);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
                #endregion
            }
            else return RedirectToAction("Login", "Account");
        }

        #region Cau hỏi khảo sát
        /// <summary>
        /// Gen câu hỏi
        /// </summary>
        /// <param name="sdt">số điện thoại</param>
        /// <param name="strDataCheck">Câu hỏi check chọn</param>
        /// <param name="strDataText">Câu hỏi trả lời</param>
        /// <returns></returns>
        private int genCauHoi(List<QuestionModelView> Get_Question, ref string strDataCheck, ref string strDataText, bool isReadonly = false)
        {
            int iCount = 0;
            string html_group = string.Empty;
            if (Get_Question.Count > 0)
            {

                foreach (var item in Get_Question)
                {
                    if (item.Answers[0].AnswerType == 0)
                    {
                        html_group = string.Empty;
                        html_group = string.Format("<div class='form-group'><label id='lbquestion{0}' class='col-md-12'><b>{1}</b><em>(*)</em></label></div>", item.QuestionID, item.QuestionText);
                        html_group += string.Format("<div class='form-group'>");
                        html_group += string.Format("<table id='table{0}' style='width: 100%'>", item.QuestionID);
                        html_group += string.Format("<thead><tr style='color:#fff'><th style='width:80%'> Nội dung </th><th style='width:10%'> Có </th><th style='width:10%'> Không </th></tr></thead>");
                        html_group += string.Format("<tbody>");
                        string row = string.Empty;
                        int i = 1;
                        foreach (var obj in item.Answers)
                        {
                            if (isReadonly)
                            {
                                //View
                                if (obj.AnswerValue == "0")
                                {
                                    row += string.Format("<tr data-i='{4}' data-questionid='{0}' data-questiontext='{1}' data-questionorder='{2}' data-answerid='{3}'>" +
                                                         "<td>{4}.{1} <em>(*)</em></td>" +
                                                         "<td><center><input type='radio' name='benh{3}' value='1' disabled /></center> </td>" +
                                                         "<td><center><input type='radio' name='benh{3}' value='0' disabled checked/></center> </td>" +
                                                   " </tr>", item.QuestionID, obj.AnswerText, item.QuestionOrder, obj.AnswerID, i);
                                }
                                else
                                {
                                    row += string.Format("<tr data-i='{4}' data-questionid='{0}' data-questiontext='{1}' data-questionorder='{2}' data-answerid='{3}'>" +
                                                        "<td>{4}.{1} <em>(*)</em></td>" +
                                                        "<td><center><input type='radio' name='benh{3}' value='1' disabled checked/></center> </td>" +
                                                        "<td><center><input type='radio' name='benh{3}' value='0' disabled /></center> </td>" +
                                                  " </tr>", item.QuestionID, obj.AnswerText, item.QuestionOrder, obj.AnswerID, i);
                                }
                            }
                            else
                            {
                                //Tạo mới
                                row += string.Format("<tr data-i='{4}' data-questionid='{0}' data-questiontext='{1}' data-questionorder='{2}' data-answerid='{3}'>" +
                                                        "<td>{4}.{1} <em>(*)</em></td>" +
                                                        "<td><center><input type='radio' name='benh{3}' value='1' /></center> </td>" +
                                                        "<td><center><input type='radio' name='benh{3}' value='0' checked /></center> </td>" +
                                                  //"<td><center><input type='radio' name='benh{3}' value='-1' /></center> </td>" +
                                                  //"<td><input type='text' maxlength='10' name='txtbenh{3}' /></td>" +
                                                  " </tr>", item.QuestionID, obj.AnswerText, item.QuestionOrder, obj.AnswerID, i);
                            }

                            i++;
                        }
                        html_group += string.Format("{0}", row);
                        html_group += string.Format("</tbody></table>");
                        html_group += string.Format("</div>");
                        strDataCheck += html_group;
                    }
                    else
                    {
                        if (isReadonly)
                        {
                            //View
                            string row = string.Format("<div class='form-group'><label id='lbquestion{0}' class='col-md-12'><b>{1}</b><em>(*)</em></label></div>" +
                                                   "<div class='form-group'><input id='txtquestion{0}' data-questionid='{0}' data-answerid='{2}' class='col-md-12' type='text' value='{3}' readonly style='cursor:not-allowed'/></div>", item.QuestionID, item.QuestionText, item.Answers[0].AnswerID, item.Answers[0].AnswerOtherText);
                            strDataText += row;
                        }
                        else
                        {
                            //Tạo mới
                            string row = string.Format("<div class='form-group'><label id='lbquestion{0}' class='col-md-12'><b>{1}</b><em>(*)</em></label></div>" +
                                                   "<div class='form-group'><input id='txtquestion{0}' data-questionid='{0}' data-answerid='{2}' class='col-md-12' type='text'/></div>", item.QuestionID, item.QuestionText, item.Answers[0].AnswerID);
                            strDataText += row;
                        }
                    }
                }
            }
            return iCount;
        }
        /// <summary>
        /// Lấy câu hỏi khảo sát
        /// </summary>
        /// <returns></returns>
        private SurveyModelForm Get_SurveyForm()
        {
            if (!string.IsNullOrEmpty(_employeeId))
            {
                using (HttpClient client = new HttpClient())
                {
                    SurveyModelForm objReturn = new SurveyModelForm();
                    client.BaseAddress = new Uri(URL_MASTER);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                    string api_name = string.Format("api/Survey/GetSurveyQuestion");
                    var objKiemtra = new InputModel();
                    var ParaMeter = new List<KeyValuePair<string, object>>();
                    ParaMeter.Add(new KeyValuePair<string, object>("empcode", _employeeId));
                    ParaMeter.Add(new KeyValuePair<string, object>("effdate", string.Format("{0:dd/MM/yyyy}", DateTime.Now)));
                    objKiemtra.ParaMeter = ParaMeter;
                    //
                    //string json_data = JToken.FromObject(objKiemtra).ToString();
                    string json_data = JToken.FromObject(objKiemtra).ToString(Newtonsoft.Json.Formatting.None);
                    var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                    Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                    HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                    try
                    {
                        response = client.PostAsync(_Uri, content).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                            if (objTmp.ResultCode == 0)
                            {
                                objReturn = JsonConvert.DeserializeObject<SurveyModelForm>(objTmp.ResultData.ToString());
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objTmp.Message))
                                {
                                    objReturn = null;
                                }
                            }
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        {
                            objReturn = null;
                        }
                        else
                        {
                            objReturn = null;

                        }
                    }
                    catch (Exception ex)
                    {
                        objReturn = null;
                    }

                    return objReturn;
                }
            }
            else
            {
                SurveyModelForm objReturn = new SurveyModelForm();
                return objReturn;
            }
        }
        private SurveyModelForm_Extend Get_SurveyFormDetail(int survey_id)
        {
            if (!string.IsNullOrEmpty(_employeeId))
            {
                using (HttpClient client = new HttpClient())
                {
                    SurveyModelForm_Extend objReturn = new SurveyModelForm_Extend();
                    client.BaseAddress = new Uri(URL_MASTER);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                    string api_name = string.Format("api/Survey/GetSurveyAnswerDetail");
                    var objKiemtra = new InputModel();
                    var ParaMeter = new List<KeyValuePair<string, object>>();
                    ParaMeter.Add(new KeyValuePair<string, object>("employeeId", _employeeId));
                    ParaMeter.Add(new KeyValuePair<string, object>("survey_id", survey_id));
                    objKiemtra.ParaMeter = ParaMeter;
                    //
                    //string json_data = JToken.FromObject(objKiemtra).ToString();
                    string json_data = JToken.FromObject(objKiemtra).ToString(Newtonsoft.Json.Formatting.None);
                    var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                    Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                    HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                    try
                    {
                        response = client.PostAsync(_Uri, content).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                            if (objTmp.ResultCode == 0)
                            {
                                objReturn = JsonConvert.DeserializeObject<SurveyModelForm_Extend>(objTmp.ResultData.ToString());
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objTmp.Message))
                                {
                                    objReturn = null;
                                }
                            }
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        {
                            objReturn = null;
                        }
                        else
                        {
                            objReturn = null;

                        }
                    }
                    catch (Exception ex)
                    {
                        objReturn = null;
                    }

                    return objReturn;
                }
            }
            else
            {
                SurveyModelForm_Extend objReturn = new SurveyModelForm_Extend();
                return objReturn;
            }
        }
        private List<HR_Survey_Answer> Get_SurveyAnswer()
        {
            if (!string.IsNullOrEmpty(_employeeId))
            {
                using (HttpClient client = new HttpClient())
                {
                    List<HR_Survey_Answer> objReturn = new List<HR_Survey_Answer>();
                    client.BaseAddress = new Uri(URL_MASTER);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                    string api_name = string.Format("api/Survey/GetSurveyAnswer");
                    var objKiemtra = new InputModel();
                    var ParaMeter = new List<KeyValuePair<string, object>>();
                    ParaMeter.Add(new KeyValuePair<string, object>("empcode", _employeeId));
                    objKiemtra.ParaMeter = ParaMeter;
                    //
                    //string json_data = JToken.FromObject(objKiemtra).ToString();
                    string json_data = JToken.FromObject(objKiemtra).ToString(Newtonsoft.Json.Formatting.None);
                    var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                    Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                    HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                    try
                    {
                        response = client.PostAsync(_Uri, content).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                            if (objTmp.ResultCode == 0)
                            {
                                objReturn = JsonConvert.DeserializeObject<List<HR_Survey_Answer>>(objTmp.ResultData.ToString());
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objTmp.Message))
                                {
                                    objReturn = null;
                                }
                            }
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        {
                            objReturn = null;
                        }
                        else
                        {
                            objReturn = null;

                        }
                    }
                    catch (Exception ex)
                    {
                        objReturn = null;
                    }

                    return objReturn;
                }
            }
            else
            {
                List<HR_Survey_Answer> objReturn = new List<HR_Survey_Answer>();
                return objReturn;
            }
        }

        #endregion

        #region Private method
        /// <summary>
        /// Lấy thông tin nhân viên
        /// </summary>
        /// <param name="employeeid"></param>
        /// <returns></returns>
        private HR_Employees Get_Employees(string employeeid)
        {
            #region Login
            if (!string.IsNullOrEmpty(employeeid))
            {
                using (HttpClient client = new HttpClient())
                {
                    HR_Employees objReturn = new HR_Employees();
                    client.BaseAddress = new Uri(URL_MASTER);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                    string api_name = string.Format("api/Survey/GetEmployee");
                    var objKiemtra = new InputModel();
                    var ParaMeter = new List<KeyValuePair<string, object>>();
                    ParaMeter.Add(new KeyValuePair<string, object>("employeeid", employeeid));
                    objKiemtra.ParaMeter = ParaMeter;
                    //
                    //string json_data = JToken.FromObject(objKiemtra).ToString();
                    string json_data = JToken.FromObject(objKiemtra).ToString(Newtonsoft.Json.Formatting.None);
                    var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                    Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                    HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                    try
                    {
                        response = client.PostAsync(_Uri, content).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                            if (objTmp.ResultCode == 0)
                            {
                                objReturn = JsonConvert.DeserializeObject<HR_Employees>(objTmp.ResultData.ToString());
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objTmp.Message))
                                {
                                    objReturn = null;
                                }
                            }
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        {
                            objReturn = null;
                        }
                        else
                        {
                            objReturn = null;

                        }
                    }
                    catch
                    {
                        objReturn = null;
                    }

                    return objReturn;
                }
            }
            else
            {
                HR_Employees objReturn = new HR_Employees();
                return objReturn;
            }

            #endregion
        }
        /// <summary>
        /// Danh sách khoa phòng
        /// </summary>
        /// <returns></returns>
        private List<HR_Department> GetKhoaphong()
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<HR_Department> objReturn = new List<HR_Department>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/Survey/GetKhoaphong");
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<HR_Department>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion
        }
        /// <summary>
        /// Danh sách tỉnh, thành phố
        /// </summary>
        /// <returns></returns>
        private List<DIC_BTDTT> Get_Btdtt()
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<DIC_BTDTT> objReturn = new List<DIC_BTDTT>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/BTDTT/Get?mavung=-1");
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<DIC_BTDTT>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion

        }
        /// <summary>
        /// Danh sách quận, huyện
        /// </summary>
        /// <returns></returns>
        private List<DIC_BTDQUAN> Get_Btdquan(string matt = "701")
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<DIC_BTDQUAN> objReturn = new List<DIC_BTDQUAN>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/BTDQUAN/Get?matt={0}", matt);
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<DIC_BTDQUAN>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion

        }
        /// <summary>
        /// Danh sách phường xã
        /// </summary>
        /// <returns></returns>
        private List<DIC_BTDPXA> Get_Btdpxa(string maqu)
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<DIC_BTDPXA> objReturn = new List<DIC_BTDPXA>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/BTDPXA/Get?maqu={0}", maqu);
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<DIC_BTDPXA>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion

        }
        /// <summary>
        /// Năm sinh
        /// </summary>
        /// <returns></returns>
        private string genNamSinh(int? namsinh)
        {

            int year = DateTime.Now.Year;
            int min = year - 70;
            int max = year - 18;

            string _html = string.Empty;
            for (int i = max; i >= min; i--)
            {

                if (namsinh.HasValue && namsinh.Value == i)
                {
                    _html += string.Format("<option value='{0}' selected>{1}</option>", i, i);
                }
                else
                {
                    _html += string.Format("<option value='{0}'>{1}</option>", i, i);
                }

            }
            return _html;
        }

        /// <summary>
        /// Giới tính
        /// </summary>
        /// <returns></returns>
        private string genPhai(int? phai)
        {
            string _html = string.Empty;

            if (phai.HasValue)
            {
                if (phai.Value == 1)
                {
                    _html = string.Format("<option value='{0}' >{1}</option>", 0, "Nam");
                    _html += string.Format("<option value='{0}' selected>{1}</option>", 1, "Nữ");
                }
                else
                {
                    _html = string.Format("<option value='{0}' selected>{1}</option>", 0, "Nam");
                    _html += string.Format("<option value='{0}'>{1}</option>", 1, "Nữ");
                }
            }
            else
            {
                _html = string.Format("<option value='{0}' selected>{1}</option>", 0, "Nam");
                _html += string.Format("<option value='{0}'>{1}</option>", 1, "Nữ");
            }

            return _html;
        }

        /// <summary>
        /// Quốc tịch
        /// </summary>
        /// <returns></returns>
        private string genQuocTich(string name)
        {
            string _html = string.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                if (!name.Equals("VN"))
                {
                    _html = string.Format("<option value='{0}' >{1}</option>", "VN", "Việt Nam");
                    _html += string.Format("<option value='{0}' selected>{1}</option>", "khac", "Khác");
                }
                else
                {
                    _html = string.Format("<option value='{0}' selected>{1}</option>", "VN", "Việt Nam");
                    _html += string.Format("<option value='{0}'>{1}</option>", "khac", "Khác");
                }
            }
            else
            {
                _html = string.Format("<option value='{0}' selected>{1}</option>", "VN", "Việt Nam");
                _html += string.Format("<option value='{0}'>{1}</option>", "khac", "Khác");
            }


            return _html;

        }

        private string Gen_Khoa(int mapk = -1)
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "department");
            List<HR_Department> lstDept = GetKhoaphong();
            if (lstDept != null && lstDept.Count > 0)
            {
                foreach (HR_Department obj in lstDept)
                {
                    if (obj.Ma == mapk)
                    {
                        _html += string.Format("<option value='{0}' selected>{1}</option>", obj.Ma, obj.Ten);
                    }
                    else
                    {
                        _html += string.Format("<option value='{0}'>{1}</option>", obj.Ma, obj.Ten);
                    }
                }
            }
            // _html = option_system.ToString();
            return _html;
        }

        /// <summary>
        /// Gen combo tinh
        /// </summary>
        /// <returns></returns>
        private string genTinh(string maTinh = "701")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdtt");
            List<DIC_BTDTT> lstBtdtt;
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                lstBtdtt = Get_Btdtt();
                if (lstBtdtt != null && lstBtdtt.Count > 0)
                {
                    foreach (var item in lstBtdtt)
                    {
                        if (item.MATT == maTinh)
                        {
                            _html += string.Format("<option value='{0}' selected>{1}</option>", item.MATT, item.TENTT);
                        }
                        else
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MATT, item.TENTT);
                        }
                    }
                    // again in memCacher Cache
                    MemoryCacheHelper.Add(_key, lstBtdtt, DateTimeOffset.UtcNow.AddDays(1));
                }

            }
            else
            {
                lstBtdtt = (List<DIC_BTDTT>)option_system;
                if (lstBtdtt != null && lstBtdtt.Count > 0)
                {
                    foreach (var item in lstBtdtt)
                    {
                        if (item.MATT == maTinh)
                        {
                            _html += string.Format("<option value='{0}' selected>{1}</option>", item.MATT, item.TENTT);
                        }
                        else
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MATT, item.TENTT);
                        }
                    }
                }
            }
            return _html;
        }

        /// <summary>
        /// Gen combo quận
        /// </summary>
        /// <param name="matt">Mã tỉnh</param>
        /// <returns></returns>
        private string genQuan(string maQuan = "-1", string matt = "701")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdquan");
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                var lstbtdQuanfull = Get_Btdquan("-1");
                //
                if (lstbtdQuanfull != null && lstbtdQuanfull.Count > 0)
                {
                    List<DIC_BTDQUAN> list = (List<DIC_BTDQUAN>)lstbtdQuanfull.Where(o => o.MATT.Equals(matt)).ToList();
                    if (list != null && list.Count != 0)
                    {
                        foreach (var item in list)
                        {
                            if (item.MAQU == maQuan)
                            {
                                _html += string.Format("<option value='{0}' selected>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                            else
                            {
                                _html += string.Format("<option value='{0}'>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                        }
                    }
                    // again in memCacher Cache
                    MemoryCacheHelper.Add(_key, lstbtdQuanfull, DateTimeOffset.UtcNow.AddDays(1));
                }
            }
            else
            {
                List<DIC_BTDQUAN> lstbtdQuanfull = (List<DIC_BTDQUAN>)option_system;
                if (lstbtdQuanfull != null && lstbtdQuanfull.Count > 0)
                {
                    List<DIC_BTDQUAN> list = (List<DIC_BTDQUAN>)lstbtdQuanfull.Where(o => o.MATT.Equals(matt)).ToList();
                    if (list != null && list.Count != 0)
                    {
                        foreach (var item in list)
                        {
                            if (item.MAQU == maQuan)
                            {
                                _html += string.Format("<option value='{0}' selected>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                            else
                            {
                                _html += string.Format("<option value='{0}'>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                        }
                    }

                }
            }
            return _html;
        }
        public string genComboQuan(string matt = "701")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdquan");
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                var lstbtdQuanfull = Get_Btdquan("-1");

                //
                if (lstbtdQuanfull != null && lstbtdQuanfull.Count > 0)
                {
                    List<DIC_BTDQUAN> list = (List<DIC_BTDQUAN>)lstbtdQuanfull.Where(p => p.MATT.Equals(matt)).ToList();
                    if (list != null && list.Count != 0)
                    {
                        foreach (var item in list)
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MAQU, item.TENQUAN);
                        }
                    }
                    // again in memCacher Cache
                    MemoryCacheHelper.Add(_key, lstbtdQuanfull, DateTimeOffset.UtcNow.AddDays(1));

                }
            }
            else
            {
                List<DIC_BTDQUAN> lstbtdQuanfull = (List<DIC_BTDQUAN>)option_system;
                if (lstbtdQuanfull != null && lstbtdQuanfull.Count > 0)
                {
                    List<DIC_BTDQUAN> list = (List<DIC_BTDQUAN>)lstbtdQuanfull.Where(o => o.MATT.Equals(matt)).ToList();
                    if (list != null && list.Count != 0)
                    {
                        foreach (var item in list)
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MAQU, item.TENQUAN);
                        }
                    }

                }
            }
            return _html;
        }

        /// <summary>
        /// Gen combo xã
        /// </summary>
        /// <param name="maqu">Mã quận</param>
        /// <returns></returns>
        private string genXa(string maXa = "-1", string maqu = "-1")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdpxa");
            var option_system = MemoryCacheHelper.GetValue(_key);
            List<DIC_BTDPXA> lstBtdpxa;
            if (option_system == null)
            {

                lstBtdpxa = Get_Btdpxa("-1");
                if (lstBtdpxa != null) MemoryCacheHelper.Add(_key, lstBtdpxa, DateTimeOffset.UtcNow.AddDays(1));
            }
            else
            {
                lstBtdpxa = (List<DIC_BTDPXA>)option_system;
            }
            if (lstBtdpxa != null && lstBtdpxa.Count > 0)
            {
                List<DIC_BTDPXA> lstDept = (List<DIC_BTDPXA>)lstBtdpxa.Where(o => o.MAQU.Equals(maqu)).ToList();
                if (lstDept != null && lstDept.Count > 0)
                {
                    foreach (var item in lstDept)
                    {
                        if (item.MAPXA.Equals(maXa))
                        { _html += string.Format("<option value='{0}' selected>{1}</option>", item.MAPXA, item.TENPXA); }
                        else
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MAPXA, item.TENPXA);
                        }

                    }
                }
            }

            return _html;
        }
        public string genComboXa(string maqu = "-1")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdpxa");
            var option_system = MemoryCacheHelper.GetValue(_key);
            List<DIC_BTDPXA> lstBtdpxa;
            if (option_system == null)
            {

                lstBtdpxa = Get_Btdpxa("-1");
                if (lstBtdpxa != null) MemoryCacheHelper.Add(_key, lstBtdpxa, DateTimeOffset.UtcNow.AddDays(1));
            }
            else
            {
                lstBtdpxa = (List<DIC_BTDPXA>)option_system;
            }
            if (lstBtdpxa != null && lstBtdpxa.Count > 0)
            {
                List<DIC_BTDPXA> lstDept = (List<DIC_BTDPXA>)lstBtdpxa.Where(o => o.MAQU.Equals(maqu)).ToList();
                if (lstDept != null && lstDept.Count > 0)
                {
                    foreach (var item in lstDept)
                    {
                        _html += string.Format("<option value='{0}'>{1}</option>", item.MAPXA, item.TENPXA);
                    }
                }
            }

            return _html;
        }
        #endregion
    }
}