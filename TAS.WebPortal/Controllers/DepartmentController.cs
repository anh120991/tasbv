﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TAS.Core.Shared;
using TAS.Core.Shared.nCoV;
using TAS.WebPortal.Helper;
using TAS.WebPortal.Provider;

namespace TAS.WebPortal.Controllers
{
    public class DepartmentController : BaseController
    {
        private readonly string URL_MASTER = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["HttpBaseAddress"]);
      //  private readonly string _Province = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultProvince"]);
        // GET: Survey
        private string _token;
        private string _token_id;
        private string _employeeId;
        /// <summary>
        /// Thông tin nhân viên của user dang nhap
        /// </summary>
        private HR_Employees_Extend employeeInfo;
        public DepartmentController()
        {

        }
        // GET: Department
        //   [Authorize(Roles = "Moderate")]
        [HttpGet]
        public ActionResult Index()
        {
            List<HR_Dept_Report> lst = new List<HR_Dept_Report>();
            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();

            lst = new nCOV(_token,null).GetList_HR_Dept_Report();

            return View(lst);
        }

       
        // GET: Department/Details/5
        //  [Authorize(Roles = "Moderate")]
        public ActionResult Details(int id)
        {
            TAS.Core.Shared.nCoV.HR_Dept_Report obj = new Core.Shared.nCoV.HR_Dept_Report();
            return View(obj);
        }

        // GET: Department/Create
    //    [Authorize(Roles = "Moderate")]
        public ActionResult Create()
        {
            //groupid == 2 mới được vào form
            if (System.Web.HttpContext.Current.Session["expires"] != null)
            {
                #region Check session
                /*Kiểm tra token còn hạn ko?*/
                string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                if (!string.IsNullOrEmpty(_date))
                {
                    DateTime date = Convert.ToDateTime(_date);
                    if (date < DateTime.Now)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
                        _employeeId = PublicSecureProvider.Decrypt(tmp);
                        //
                        _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
                        _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                        employeeInfo = GetHR_Employees(_employeeId);
                        if (employeeInfo != null)
                        {
                            List<HR_Employees_Extend> empList = Get_EmployeesFull_By_Makp(employeeInfo.Makp);
                            ViewData["TENKP"] = employeeInfo.Tenkp.ToUpper();
                            HR_Dept_Report obj = new HR_Dept_Report()
                            {
                                Makp=employeeInfo.Makp,
                                Tenkp=employeeInfo.Tenkp,
                                Nguoibc=employeeInfo.Hoten
                            };
                            return View(obj);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }

                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
                #endregion
            }
            else return RedirectToAction("Login", "Account");
            
        }

        // POST: Department/Create
     //   [Authorize(Roles = "Moderate")]
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                HR_Dept_Report obj = new HR_Dept_Report();

                if (!string.IsNullOrEmpty(collection["txtNgayBC"]))
                {
                    DateTime dtNgaybc = Convert.ToDateTime(collection["txtNgayBC"]);

                    obj.Ngaybc = dtNgaybc.ToString("dd/MM/yyyy");
                }

                if (!string.IsNullOrEmpty(collection["txtMakp"])) obj.Makp = Convert.ToInt32(collection["txtMakp"]);
                if (!string.IsNullOrEmpty(collection["txtTenkp"])) obj.Tenkp = Convert.ToString(collection["txtTenkp"]);
                //if (!string.IsNullOrEmpty(collection["txtNgayBC"])) obj.Ngaybc = Convert.ToString(collection["txtNgayBC"]);
                if (!string.IsNullOrEmpty(collection["txtQuanSo"])) obj.Quanso = Convert.ToInt32(collection["txtQuanSo"]);
                if (!string.IsNullOrEmpty(collection["txtHienCo"])) obj.Hienco = Convert.ToInt32(collection["txtHienCo"]);
                if (!string.IsNullOrEmpty(collection["txtHocViec"])) obj.Vang = Convert.ToInt32(collection["txtHocViec"]);
                if (!string.IsNullOrEmpty(collection["txtVang"])) obj.Hocviec = Convert.ToInt32(collection["txtVang"]);
                if (!string.IsNullOrEmpty(collection["txtLydoVang"])) obj.Lydovang = Convert.ToString(collection["txtLydoVang"]);
                if (!string.IsNullOrEmpty(collection["txtKhaibao"])) obj.Khaibao = Convert.ToInt32(collection["txtKhaibao"]);
                if (!string.IsNullOrEmpty(collection["txtBoSung"])) obj.Bosung = Convert.ToInt32(collection["txtBoSung"]);
                
                if (!string.IsNullOrEmpty(collection["txtKhac"])) obj.Khac = Convert.ToString(collection["txtKhac"]);
                if (!string.IsNullOrEmpty(collection["txtDeXuat"])) obj.Dexuat = Convert.ToString(collection["txtDeXuat"]);

                if (!string.IsNullOrEmpty(collection["txtNguoiBaoCao"])) obj.Nguoibc = Convert.ToString(collection["txtNguoiBaoCao"]);
                if (!string.IsNullOrEmpty(collection[""])) obj.Trangthai = 0;
                //if (!string.IsNullOrEmpty(collection[""])) obj.CreatedUser = Convert.ToInt32(collection[""]);

                _token = System.Web.HttpContext.Current.Session["access_token"].ToString();

                var bol = new nCOV(_token,null).Add_HR_Dept_Report(obj);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Department/Edit/5
        [HttpGet]
      //  [Authorize(Roles = "Moderate")]
        public ActionResult Edit(int id)
        {
            //groupid == 2 mới được vào form

            TAS.Core.Shared.nCoV.HR_Dept_Report obj = new Core.Shared.nCoV.HR_Dept_Report()
            {
                Id = 1,
                Makp = 1,
                Tenkp = "Phòng khám nội tổng quát",
                Stt = 1,
                Quanso = 100,
                Hienco = 95,
                Vang = 5,
                Hocviec = 10,
                Lydovang = "Không thích học nữa nghỉ >_<",
                Trangthai = 0
            };
            return View(obj);
        }

        // POST: Department/Edit/5
        [HttpPost]
      //  [Authorize(Roles = "Moderate")]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            TAS.Core.Shared.nCoV.HR_Dept_Report obj = new Core.Shared.nCoV.HR_Dept_Report()
            {
                Id = 1,
                Makp = 1,
                Tenkp = "Phòng khám nội tổng quát",
                Stt = 1,
                Quanso = 100,
                Hienco = 95,
                Vang = 5,
                Hocviec = 10,
                Lydovang = "Không thích học nữa nghỉ >_<",
                Trangthai = 0
            };
            return View(obj);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult GetEmployees()
        {
            if (System.Web.HttpContext.Current.Session["expires"] != null)
            {
                #region Check session
                /*Kiểm tra token còn hạn ko?*/
                string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                if (!string.IsNullOrEmpty(_date))
                {
                    DateTime date = Convert.ToDateTime(_date);
                    if (date < DateTime.Now)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
                        _employeeId = PublicSecureProvider.Decrypt(tmp);
                        //
                        _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
                        _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                        employeeInfo = GetHR_Employees(_employeeId);
                        if (employeeInfo != null)
                        {
                            List<HR_Employees_Extend> empList = Get_EmployeesFull_By_Makp(employeeInfo.Makp);
                            ViewData["TENKP"] = employeeInfo.Tenkp.ToUpper();
                            ViewBag.Makp = this.Gen_Khoa(employeeInfo.Makp);
                           // ViewData["EMPLIST"] = empList;
                            return View(empList);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                       
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
                #endregion
            }
            else return RedirectToAction("Login", "Account");
        }
        
        [HttpPost]
        public ActionResult GetEmployeesByDept(int makp = -1)
        {
            if (Request.IsAjaxRequest())
            {
                if (System.Web.HttpContext.Current.Session["expires"] != null)
                {
                    #region Check session
                    /*Kiểm tra token còn hạn ko?*/
                    string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                    if (!string.IsNullOrEmpty(_date))
                    {
                        DateTime date = Convert.ToDateTime(_date);
                        if (date < DateTime.Now)
                        {
                            return RedirectToAction("Login", "Account");
                        }
                        else
                        {
                            var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
                            _employeeId = PublicSecureProvider.Decrypt(tmp);
                            //
                            _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
                            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                            List<HR_Employees_Extend> list = Get_EmployeesFull_By_Makp(makp);
                            return PartialView("_EmployeeListPartial", list);

                        }
                    }
                    else
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    #endregion
                }
                else return RedirectToAction("Login", "Account");

               
            }
            else             return View();
        }

        public string genEmployees(int makp = -1)
        {
            var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
            _employeeId = PublicSecureProvider.Decrypt(tmp);
            //
            _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();

            List<HR_Employees_Extend> lst = Get_EmployeesFull_By_Makp(makp);
            string strData = string.Empty;
            int i = 1;
            if (makp != -2)
            {
                if (lst != null && lst.Count > 0)
                {
                    foreach (var item in lst)
                    {
                        strData += string.Format("<tr>" +
                                                  "<td> {0} </td>" +
                                                  "<td> {1} </td>" +
                                                  "<td> {2} </td>" +
                                                  "<td> {3} </td>" +
                                                  "<td> {4} </td>" +
                                                 "</tr>", i, item.Hoten, item.Namsinh, item.Gioitinh, item.Sonha);
                        i++;
                    }
                }
                else
                {
                    strData = "<tr> <td colspan='5'> Không có dữ liệu</td> </tr>";
                }
            }
            else
            {
                strData = "<tr> <td colspan='5'> Vui lòng chọn khoa phòng cần xem </td> </tr>";
            }
            return strData;
        }

        #region Private Method
        private string Gen_Khoa(int mapk)
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "department");
            List<HR_Department> lstDept=new List<HR_Department>();//= GetKhoaphong();
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                lstDept= GetKhoaphong();
                // again in memCacher Cache
                MemoryCacheHelper.Add(_key, lstDept, DateTimeOffset.UtcNow.AddDays(1));
            }
            else
            {
                lstDept = (List<HR_Department>)option_system;
            }
            if (lstDept != null && lstDept.Count > 0)
            {
                if (User.GroupID == 1)
                {
                    #region Administrator
                    _html = string.Format("<option value='{0}' selected>{1}</option>", -1,"Toàn viện");
                    foreach (HR_Department obj in lstDept)
                    {
                        if (obj.Ma == mapk)
                        {
                            _html += string.Format("<option value='{0}' selected>{1}</option>", obj.Ma, obj.Ten);
                        }
                        else
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", obj.Ma, obj.Ten);
                        }
                    }
                    #endregion
                }
                else
                {
                    var find = lstDept.Find(o => o.Ma.Equals(mapk));
                    if (find != null)
                    {
                        _html += string.Format("<option value='{0}' selected>{1}</option>", find.Ma, find.Ten);
                    }
                    else _html = string.Empty;
                }

            }
            // _html = option_system.ToString();
            return _html;
        }
        /// <summary>
        /// Danh sách khoa phòng
        /// </summary>
        /// <returns></returns>
        private List<HR_Department> GetKhoaphong()
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<HR_Department> objReturn = new List<HR_Department>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/Survey/GetKhoaphong");
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<HR_Department>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion
        }
        private List<HR_Dept_Report> Get_Dept_Reports(int makp, string ngay)
        {
            List<HR_Dept_Report> list = new List<HR_Dept_Report>();

            return list;
        }

        /// <summary>
        /// Danh sách nhân viên
        /// </summary>
        /// <param name="makp"></param>
        /// <returns></returns>
        private List<HR_Employees_Extend> Get_EmployeesFull_By_Makp(int makp)
        { 
            using (HttpClient client = new HttpClient())
            {
                List<HR_Employees_Extend> objReturn = new List<HR_Employees_Extend>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/Department/GetEmployeeByDept?makp={0}",makp);               
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<HR_Employees_Extend>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch (Exception ex)
                {
                    objReturn = null;
                }

                return objReturn;
            }
        }
        /// <summary>
        /// Thông tin nhân viên
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        private HR_Employees_Extend GetHR_Employees(string employeeId)
        {
            if (!string.IsNullOrEmpty(employeeId))
            {
                using (HttpClient client = new HttpClient())
                {
                    HR_Employees_Extend objReturn = new HR_Employees_Extend();
                    client.BaseAddress = new Uri(URL_MASTER);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                    string api_name = string.Format("api/Survey/GetEmployeeInfo");
                    var objKiemtra = new InputModel();
                    var ParaMeter = new List<KeyValuePair<string, object>>();
                    ParaMeter.Add(new KeyValuePair<string, object>("empcode", employeeId));
                    objKiemtra.ParaMeter = ParaMeter;
                    //
                    //string json_data = JToken.FromObject(objKiemtra).ToString();
                    string json_data = JToken.FromObject(objKiemtra).ToString(Newtonsoft.Json.Formatting.None);
                    var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                    Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                    HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                    try
                    {
                        response = client.PostAsync(_Uri, content).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                            if (objTmp.ResultCode == 0)
                            {
                                objReturn = JsonConvert.DeserializeObject<HR_Employees_Extend>(objTmp.ResultData.ToString());
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objTmp.Message))
                                {
                                    objReturn = null;
                                }
                            }
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        {
                            objReturn = null;
                        }
                        else
                        {
                            objReturn = null;

                        }
                    }
                    catch (Exception ex)
                    {
                        objReturn = null;
                    }

                    return objReturn;
                }
            }
            else
            {
                HR_Employees_Extend objReturn = new HR_Employees_Extend();
                return objReturn;
            }
        }
        #endregion
    }
}
