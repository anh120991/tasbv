﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TAS.Core.Entities.MasterData.Administrative;
using TAS.Core.Shared;
using TAS.Core.Shared.nCoV;
using TAS.WebPortal.Helper;
using TAS.WebPortal.Models;
using TAS.WebPortal.Provider;

namespace TAS.WebPortal.Controllers
{
    [Authorize]
    public class ManageController : BaseController
    {
        private readonly string URL_MASTER = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["HttpBaseAddress"]);
        private readonly string _Province = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultProvince"]);

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        private string _token;
        private string _token_id;
        private string _employeeId;
        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Manage/Index
        //public async Task<ActionResult> Index(ManageMessageId? message)
        //{
        //    ViewBag.StatusMessage =
        //        message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
        //        : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
        //        : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
        //        : message == ManageMessageId.Error ? "An error has occurred."
        //        : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
        //        : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
        //        : "";

        //    var userId = User.Identity.GetUserName();
        //    var model = new IndexViewModel
        //    {
        //        HasPassword = HasPassword(),
        //        PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
        //        TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
        //        Logins = await UserManager.GetLoginsAsync(userId),
        //        BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId)
        //    };
        //    return View(model);
        //}

        public ActionResult Index()
        {
            if (System.Web.HttpContext.Current.Session["expires"] != null)
            {
                #region Check session
                /*Kiểm tra token còn hạn ko?*/
                string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                if (!string.IsNullOrEmpty(_date))
                {
                    DateTime date = Convert.ToDateTime(_date);
                    if (date < DateTime.Now)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
                        _employeeId = PublicSecureProvider.Decrypt(tmp);
                        //
                        _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
                        _token = System.Web.HttpContext.Current.Session["access_token"].ToString();


                        ViewData["EFFDATE"] = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                        string strData1 = string.Empty;
                        string strData2 = string.Empty;
                        var objSurveyForm = Get_SurveyForm();
                        //
                        if (!string.IsNullOrEmpty(objSurveyForm.Employee.Namsinh))
                        {
                            ViewBag.NamSinh = genNamSinh(int.Parse(objSurveyForm.Employee.Namsinh));
                        }
                        else
                        {
                            ViewBag.NamSinh = genNamSinh(null);
                        }


                        ViewBag.Phai = genPhai(objSurveyForm.Employee.Phai);
                        ViewBag.QuocTich = genQuocTich(objSurveyForm.Employee.Quoctich.Trim());
                        ViewBag.KhoaPhong = Gen_Khoa(objSurveyForm.Employee.Makp);
                        ViewBag.Tinh = genTinh(objSurveyForm.Employee.Matt);
                        ViewBag.Quan = genQuan(objSurveyForm.Employee.Maqu, objSurveyForm.Employee.Matt);
                        ViewBag.Xa = genXa(objSurveyForm.Employee.Mapxa, objSurveyForm.Employee.Maqu);
                        ViewBag.CauHoiChon = strData1;
                        ViewBag.CauHoi = strData2;
                        //
                        return View(objSurveyForm);

                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
                #endregion
            }
            else return RedirectToAction("Login", "Account");

           // return View();
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        //
        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /Manage/ChangePassword
        [HttpGet]
        [Authorize]
        public ActionResult ChangePassword(int type,bool require)
        {
            
            //return View();
            if (System.Web.HttpContext.Current.Session["expires"] != null)
            {
                #region Check session
                /*Kiểm tra token còn hạn ko?*/
                string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                if (!string.IsNullOrEmpty(_date))
                {
                    DateTime date = Convert.ToDateTime(_date);
                    if (date < DateTime.Now)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        ChangePasswordViewModel model = new ChangePasswordViewModel() 
                        {
                            ChangePasswordType=type,
                            IsRequireChange=require
                        
                        };
                        ViewData["IsRequireChange"] = require;
                        ViewData["ChangePasswordType"] = type;
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
                #endregion
            }
            else return RedirectToAction("Login", "Account");

        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            bool isUpdate = false;
            int i_type = model.ChangePasswordType;
            if (System.Web.HttpContext.Current.Session["expires"] != null)
            {
                #region Check session
                /*Kiểm tra token còn hạn ko?*/
                string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                if (!string.IsNullOrEmpty(_date))
                {
                    DateTime date = Convert.ToDateTime(_date);
                    if (date < DateTime.Now)
                    {
                        ModelState.AddModelError("", "Hết phiên đăng nhập!");
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
                        _employeeId = PublicSecureProvider.Decrypt(tmp);
                        //
                        _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
                        _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                        try
                        {

                            isUpdate = await this.ChangePassword(i_type, _employeeId, model.ConfirmPassword, _token, _employeeId);
                            if (isUpdate)
                            {

                                #region Xóa cookie, cache
                                IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;
                                authenticationManager.SignOut();
                                Session.Abandon();

                                // Clear authentication cookie
                                HttpCookie rFormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                                rFormsCookie.Expires = DateTime.Now.AddYears(-1);
                                Response.Cookies.Add(rFormsCookie);

                                // Clear session cookie 
                                HttpCookie rSessionCookie = new HttpCookie("ASP.NET_SessionId", "");
                                rSessionCookie.Expires = DateTime.Now.AddYears(-1);
                                Response.Cookies.Add(rSessionCookie);
                                //
                                System.Web.HttpContext.Current.Session["access_token"] = string.Empty;
                                System.Web.HttpContext.Current.Session["expires"] = string.Empty;
                                System.Web.HttpContext.Current.Session["token_id"] = string.Empty;
                                System.Web.HttpContext.Current.Session["unique_id"] = string.Empty;
                                System.Web.HttpContext.Current.Session["isrequire"] = string.Empty;
                                // Invalidate the Cache on the Client Side
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.Cache.SetNoStore();

                                return RedirectToAction("ChangePasswordSuccess","Manage");
                                #endregion

                            }
                            else
                            {
                                ModelState.AddModelError("", "Thay đổi mật khẩu không thành công!\n Vui lòng thử lại!");
                                return View(model);
                            }

                        }
                        catch (Exception objEx)
                        {
                            ModelState.AddModelError("", "Thay đổi mật khẩu không thành công!\n Vui lòng thử lại!");
                            return View(model);
                        }

                    }
                }
                else
                {
                    ModelState.AddModelError("", "Hết phiên đăng nhập!");
                    return RedirectToAction("Login", "Account");
                }
                #endregion
            }
            else
            {
                ModelState.AddModelError("", "Hết phiên đăng nhập!");
                return RedirectToAction("Login", "Account");
            }
             
          
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }
        #region Change Password
        /// <summary>
        /// Thay đổi mật khẩu
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="access_token"></param>
        /// <returns></returns>
        private Task<bool> ChangePassword(int type,string username, string password, string access_token,string userid)
        {
            bool bok = false;
            string URL_MASTER = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["HttpBaseAddress"]);
            string api_name = "api/Account/ChangePassword";
            string _deserialize = string.Empty;
            string _message = string.Empty;
            #region Change pass
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // HTTP POST
                var input = new InputModel();
                List<KeyValuePair<string, object>> para = new List<KeyValuePair<string, object>>();
                para.Add(new KeyValuePair<string, object>("type", type));
                para.Add(new KeyValuePair<string, object>("username", username));
                para.Add(new KeyValuePair<string, object>("password", password));
                para.Add(new KeyValuePair<string, object>("userid", userid));
                input.ParaMeter = para;
                string json_data = JToken.FromObject(input).ToString();
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");

                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                try
                {
                    HttpResponseMessage response = client.PostAsync(_Uri, content).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        var KQ=  JsonConvert.DeserializeObject<APIResult>(result.ToString());
                        if (KQ.ResultCode==0) return Task.FromResult(true);
                        else return Task.FromResult(false);
                    }
                    else
                    {

                        return Task.FromResult(false);
                    }
                }
                catch (Exception ex)
                {
                    return Task.FromResult(false);
                }
            }
            #endregion
        }
        #endregion
        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }
        
        public JsonResult Update(string objHR_Employees)
        {
            HR_Employees obj = JsonConvert.DeserializeObject<HR_Employees>(objHR_Employees);
            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
            try
            {
                bool isUpdate = new nCOV(_token,null).Update_HR_Employee(obj);

                return Json(new { iserror = isUpdate }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception objEx)
            {
                return Json(new { content = objEx, iserror = false }, JsonRequestBehavior.AllowGet);
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindByName(User.Identity.GetUserName());
            
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion

        #region Private method
        /// <summary>
        /// Lấy câu hỏi khảo sát
        /// </summary>
        /// <returns></returns>
        private SurveyModelForm Get_SurveyForm()
        {
            if (!string.IsNullOrEmpty(_employeeId))
            {
                using (HttpClient client = new HttpClient())
                {
                    SurveyModelForm objReturn = new SurveyModelForm();
                    client.BaseAddress = new Uri(URL_MASTER);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                    string api_name = string.Format("api/Survey/GetSurveyQuestion");
                    var objKiemtra = new InputModel();
                    var ParaMeter = new List<KeyValuePair<string, object>>();
                    ParaMeter.Add(new KeyValuePair<string, object>("empcode", _employeeId));
                    ParaMeter.Add(new KeyValuePair<string, object>("effdate", string.Format("{0:dd/MM/yyyy}", DateTime.Now)));
                    objKiemtra.ParaMeter = ParaMeter;
                    //
                    //string json_data = JToken.FromObject(objKiemtra).ToString();
                    string json_data = JToken.FromObject(objKiemtra).ToString(Newtonsoft.Json.Formatting.None);
                    var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                    Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                    HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                    try
                    {
                        response = client.PostAsync(_Uri, content).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                            if (objTmp.ResultCode == 0)
                            {
                                objReturn = JsonConvert.DeserializeObject<SurveyModelForm>(objTmp.ResultData.ToString());
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objTmp.Message))
                                {
                                    objReturn = null;
                                }
                            }
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        {
                            objReturn = null;
                        }
                        else
                        {
                            objReturn = null;

                        }
                    }
                    catch (Exception ex)
                    {
                        objReturn = null;
                    }

                    return objReturn;
                }
            }
            else
            {
                SurveyModelForm objReturn = new SurveyModelForm();
                return objReturn;
            }
        }

        /// <summary>
        /// Lấy thông tin nhân viên
        /// </summary>
        /// <param name="employeeid"></param>
        /// <returns></returns>
        private HR_Employees Get_Employees(string employeeid)
        {
            #region Login
            if (!string.IsNullOrEmpty(employeeid))
            {
                using (HttpClient client = new HttpClient())
                {
                    HR_Employees objReturn = new HR_Employees();
                    client.BaseAddress = new Uri(URL_MASTER);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                    string api_name = string.Format("api/Survey/GetEmployee");
                    var objKiemtra = new InputModel();
                    var ParaMeter = new List<KeyValuePair<string, object>>();
                    ParaMeter.Add(new KeyValuePair<string, object>("employeeid", employeeid));
                    objKiemtra.ParaMeter = ParaMeter;
                    //
                    //string json_data = JToken.FromObject(objKiemtra).ToString();
                    string json_data = JToken.FromObject(objKiemtra).ToString(Newtonsoft.Json.Formatting.None);
                    var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                    Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                    HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                    try
                    {
                        response = client.PostAsync(_Uri, content).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                            if (objTmp.ResultCode == 0)
                            {
                                objReturn = JsonConvert.DeserializeObject<HR_Employees>(objTmp.ResultData.ToString());
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objTmp.Message))
                                {
                                    objReturn = null;
                                }
                            }
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        {
                            objReturn = null;
                        }
                        else
                        {
                            objReturn = null;

                        }
                    }
                    catch
                    {
                        objReturn = null;
                    }

                    return objReturn;
                }
            }
            else
            {
                HR_Employees objReturn = new HR_Employees();
                return objReturn;
            }

            #endregion
        }
        /// <summary>
        /// Danh sách khoa phòng
        /// </summary>
        /// <returns></returns>
        private List<HR_Department> GetKhoaphong()
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<HR_Department> objReturn = new List<HR_Department>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/Survey/GetKhoaphong");
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<HR_Department>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion
        }
        /// <summary>
        /// Danh sách tỉnh, thành phố
        /// </summary>
        /// <returns></returns>
        private List<DIC_BTDTT> Get_Btdtt()
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<DIC_BTDTT> objReturn = new List<DIC_BTDTT>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/BTDTT/Get?mavung=-1");
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<DIC_BTDTT>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion

        }
        /// <summary>
        /// Danh sách quận, huyện
        /// </summary>
        /// <returns></returns>
        private List<DIC_BTDQUAN> Get_Btdquan(string matt = "701")
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<DIC_BTDQUAN> objReturn = new List<DIC_BTDQUAN>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/BTDQUAN/Get?matt={0}", matt);
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<DIC_BTDQUAN>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion

        }
        /// <summary>
        /// Danh sách phường xã
        /// </summary>
        /// <returns></returns>
        private List<DIC_BTDPXA> Get_Btdpxa(string maqu)
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<DIC_BTDPXA> objReturn = new List<DIC_BTDPXA>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/BTDPXA/Get?maqu={0}", maqu);
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<DIC_BTDPXA>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion

        }
        /// <summary>
        /// Năm sinh
        /// </summary>
        /// <returns></returns>
        private string genNamSinh(int? namsinh)
        {

            int year = DateTime.Now.Year;
            int min = year - 70;
            int max = year - 18;

            string _html = string.Empty;
            for (int i = max; i >= min; i--)
            {

                if (namsinh.HasValue && namsinh.Value == i)
                {
                    _html += string.Format("<option value='{0}' selected>{1}</option>", i, i);
                }
                else
                {
                    _html += string.Format("<option value='{0}'>{1}</option>", i, i);
                }

            }
            return _html;
        }

        /// <summary>
        /// Giới tính
        /// </summary>
        /// <returns></returns>
        private string genPhai(int? phai)
        {
            string _html = string.Empty;

            if (phai.HasValue)
            {
                if (phai.Value == 1)
                {
                    _html = string.Format("<option value='{0}' >{1}</option>", 0, "Nam");
                    _html += string.Format("<option value='{0}' selected>{1}</option>", 1, "Nữ");
                }
                else
                {
                    _html = string.Format("<option value='{0}' selected>{1}</option>", 0, "Nam");
                    _html += string.Format("<option value='{0}'>{1}</option>", 1, "Nữ");
                }
            }
            else
            {
                _html = string.Format("<option value='{0}' selected>{1}</option>", 0, "Nam");
                _html += string.Format("<option value='{0}'>{1}</option>", 1, "Nữ");
            }

            return _html;
        }

        /// <summary>
        /// Quốc tịch
        /// </summary>
        /// <returns></returns>
        private string genQuocTich(string name)
        {
            string _html = string.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                if (!name.Equals("VN"))
                {
                    _html = string.Format("<option value='{0}' >{1}</option>", "VN", "Việt Nam");
                    _html += string.Format("<option value='{0}' selected>{1}</option>", "khac", "Khác");
                }
                else
                {
                    _html = string.Format("<option value='{0}' selected>{1}</option>", "VN", "Việt Nam");
                    _html += string.Format("<option value='{0}'>{1}</option>", "khac", "Khác");
                }
            }
            else
            {
                _html = string.Format("<option value='{0}' selected>{1}</option>", "VN", "Việt Nam");
                _html += string.Format("<option value='{0}'>{1}</option>", "khac", "Khác");
            }


            return _html;

        }

        private string Gen_Khoa(int mapk = -1)
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "department");
            List<HR_Department> lstDept = GetKhoaphong();
            //var option_system = MemoryCacheHelper.GetValue(_key);
            //if (option_system == null)
            //{
            //    lstDept = GetKhoaphong();

            //    // again in memCacher Cache
            //    MemoryCacheHelper.Add(_key, option_system, DateTimeOffset.UtcNow.AddHours(2));
            //}
            //else
            //{
            //    lstDept = (List<HR_Department>)option_system;
            //}

            if (lstDept != null && lstDept.Count > 0)
            {
                foreach (HR_Department obj in lstDept)
                {
                    if (obj.Ma == mapk)
                    {
                        _html += string.Format("<option value='{0}' selected>{1}</option>", obj.Ma, obj.Ten);
                    }
                    else
                    {
                        _html += string.Format("<option value='{0}'>{1}</option>", obj.Ma, obj.Ten);
                    }
                }
            }
            // _html = option_system.ToString();
            return _html;

        }

        /// <summary>
        /// Gen combo tinh
        /// </summary>
        /// <returns></returns>
        private string genTinh(string maTinh = "701")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdtt");
            List<DIC_BTDTT> lstBtdtt;
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                lstBtdtt = Get_Btdtt();
                if (lstBtdtt != null && lstBtdtt.Count > 0)
                {
                    foreach (var item in lstBtdtt)
                    {
                        if (item.MATT == maTinh)
                        {
                            _html += string.Format("<option value='{0}' selected>{1}</option>", item.MATT, item.TENTT);
                        }
                        else
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MATT, item.TENTT);
                        }
                    }
                }
                // again in memCacher Cache
                MemoryCacheHelper.Add(_key, lstBtdtt, DateTimeOffset.UtcNow.AddDays(1));
            }
            else
            {
                lstBtdtt = (List<DIC_BTDTT>)option_system;
                if (lstBtdtt != null && lstBtdtt.Count > 0)
                {
                    foreach (var item in lstBtdtt)
                    {
                        if (item.MATT == maTinh)
                        {
                            _html += string.Format("<option value='{0}' selected>{1}</option>", item.MATT, item.TENTT);
                        }
                        else
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MATT, item.TENTT);
                        }
                    }
                }
            }
            return _html;
        }

        /// <summary>
        /// Gen combo quận
        /// </summary>
        /// <param name="matt">Mã tỉnh</param>
        /// <returns></returns>
        private string genQuan(string maQuan = "-1", string matt = "701")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdquan");
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                var lstbtdQuanfull = Get_Btdquan("-1");
                // again in memCacher Cache
                MemoryCacheHelper.Add(_key, lstbtdQuanfull, DateTimeOffset.UtcNow.AddDays(1));

                //
                if (lstbtdQuanfull != null && lstbtdQuanfull.Count > 0)
                {
                    List<DIC_BTDQUAN> list = (List<DIC_BTDQUAN>)lstbtdQuanfull.Where(o => o.MATT.Equals(matt)).ToList();
                    if (list != null && list.Count != 0)
                    {
                        foreach (var item in list)
                        {
                            if (item.MAQU == maQuan)
                            {
                                _html += string.Format("<option value='{0}' selected>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                            else
                            {
                                _html += string.Format("<option value='{0}'>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                        }
                    }

                }
            }
            else
            {
                List<DIC_BTDQUAN> lstbtdQuanfull = (List<DIC_BTDQUAN>)option_system;
                if (lstbtdQuanfull != null && lstbtdQuanfull.Count > 0)
                {
                    List<DIC_BTDQUAN> list = (List<DIC_BTDQUAN>)lstbtdQuanfull.Where(o => o.MATT.Equals(matt)).ToList();
                    if (list != null && list.Count != 0)
                    {
                        foreach (var item in list)
                        {
                            if (item.MAQU == maQuan)
                            {
                                _html += string.Format("<option value='{0}' selected>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                            else
                            {
                                _html += string.Format("<option value='{0}'>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                        }
                    }

                }
            }
            return _html;
        }

        /// <summary>
        /// Gen combo xã
        /// </summary>
        /// <param name="maqu">Mã quận</param>
        /// <returns></returns>
        private string genXa(string maXa = "-1", string maqu = "-1")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdpxa");
            var option_system = MemoryCacheHelper.GetValue(_key);
            List<DIC_BTDPXA> lstBtdpxa;
            if (option_system == null)
            {

                lstBtdpxa = Get_Btdpxa("-1");
                MemoryCacheHelper.Add(_key, lstBtdpxa, DateTimeOffset.UtcNow.AddDays(1));
            }
            else
            {
                lstBtdpxa = (List<DIC_BTDPXA>)option_system;
            }
            if (lstBtdpxa != null && lstBtdpxa.Count > 0)
            {
                List<DIC_BTDPXA> lstDept = (List<DIC_BTDPXA>)lstBtdpxa.Where(o => o.MAQU.Equals(maqu)).ToList();
                if (lstDept != null && lstDept.Count > 0)
                {
                    foreach (var item in lstDept)
                    {
                        if (item.MAPXA.Equals(maXa))
                        { _html += string.Format("<option value='{0}' selected>{1}</option>", item.MAPXA, item.TENPXA); }
                        else
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MAPXA, item.TENPXA);
                        }

                    }
                }
            }

            return _html;
        }
        #endregion
    }
}