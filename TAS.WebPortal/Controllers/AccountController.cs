﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using TAS.WebPortal.Models; 
using TAS.Core.Shared;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Reflection;
 

namespace TAS.WebPortal.Controllers
{
    [Authorize]
   // [ValidateAntiForgeryToken]
    public class AccountController : Controller
    {
        //https://stackoverflow.com/questions/1064271/asp-net-mvc-set-custom-iidentity-or-iprincipal?rq=1
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly List<ApplicationRole> Roles;
        public AccountController()
        {
            Roles = CreateRoles();

        }
        private List<ApplicationRole> CreateRoles()
        {
            List<ApplicationRole>  list = new List<ApplicationRole>();
            list.Add(new ApplicationRole() { Id="1",Name="Administrator"});
            list.Add(new ApplicationRole() { Id = "2", Name = "Moderator" });
            list.Add(new ApplicationRole() { Id = "3", Name = "Normal" });
            return list;
        }
        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
            Roles = CreateRoles();
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        [HttpGet]
    //    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]

        public ActionResult Login(string returnUrl)
        {
            LoginViewModel model = new LoginViewModel();
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
       // [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
           
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            #region Kiểm tra trong cookie có chưa
            //ApplicationUser objUser = UserManager.Find(model.Username, model.Password);
            //if (objUser != null)
            //{
            //    IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;
            //    authenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            //    ClaimsIdentity identity = UserManager.CreateIdentity(objUser, DefaultAuthenticationTypes.ApplicationCookie);
            //    AuthenticationProperties props = new AuthenticationProperties();
            //    props.IsPersistent = model.RememberMe;
            //    authenticationManager.SignIn(props, identity);
            //    if (Url.IsLocalUrl(returnUrl))
            //    {
            //        return Redirect(returnUrl);
            //    }
            //    else
            //    {
            //        return RedirectToAction("Index", "Survey");
            //    }
            //}
            #endregion
            #region Chưa có
            var _userProfile = new UserProfile();
            string token_id = string.Empty;
            string access_token = string.Empty;
            var resultDB = await this.LoginSystemAPI(model.Username, model.Password, ref _userProfile,ref token_id,ref access_token);
            if (resultDB)
            {
                if (!_userProfile.IsValidProfile)
                {
                    ModelState.AddModelError("", "Người dùng bị khóa quyền truy cập! Không cho phép truy cập!");
                    return View(model);
                }              
                else
                {
                    CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel()
                    {
                        Token_ID = token_id,
                        Fullname = _userProfile.hoten,
                        GroupID = _userProfile.groupid,
                        UserID = _userProfile.userid,
                        Access_token = access_token,
                        BranchID = _userProfile.defaultBranchID,
                        BranchName=_userProfile.defaultBranchName,
                    };
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string userData = serializer.Serialize(serializeModel);
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                             1,
                             model.Username,
                             DateTime.Now,
                             DateTime.Now.AddMinutes(10),
                             false,
                             userData);
                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Add(faCookie);
                    if (_userProfile.RequireChangePassword)
                    {
                        return RedirectToAction("ChangePassword", "Manage", new { type = 2, require = true });
                    }
                    else if (_userProfile.groupid == 2)
                    {
                        return RedirectToAction("Index", "Home",new { Area = "Branch" });
                    }
                    else
                    {
                        return RedirectToAction("Create", "Survey");
                    }
                        
                }
               
            }
            else
            {
                ModelState.AddModelError("", "Đăng nhập không thành công!");
                return View(model);
            }
            #endregion


        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);
                    
                    // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        #region Namnp -  Change password
      
        //[HttpPost]
        //[Authorize]
        //[ValidateAntiForgeryToken]
        //public async Task<JsonResult> ChangePassword(InputModel model)
        //{
        //    bool isUpdate = false;
        //    if (ModelState.IsValid)
        //    {
        //        if (System.Web.HttpContext.Current.Session["expires"] != null)
        //        {
        //            #region Check session
        //            /*Kiểm tra token còn hạn ko?*/
        //            string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
        //            if (!string.IsNullOrEmpty(_date))
        //            {
        //                DateTime date = Convert.ToDateTime(_date);
        //                if (date < DateTime.Now)
        //                {
        //                    return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);
        //                }
        //                else
        //                {
        //                    var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
        //                    var _employeeId = PublicSecureProvider.Decrypt(tmp);
        //                    //
        //                    var _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
        //                    var _token = System.Web.HttpContext.Current.Session["access_token"].ToString();

        //                    var _old_pass = model.ParaMeter[0].Value.ToString();
        //                    var _newpass= model.ParaMeter[1].Value.ToString();
        //                    //if (!_username.Equals(_employeeId))
        //                    //{
        //                    //    return Json(new { content = "Tên đăng nhập không đúng!\nVui lòng thử lại", iserror = false }, JsonRequestBehavior.AllowGet);
        //                    //}
        //                    try
        //                    {
        //                        isUpdate = await ChangePassword(_employeeId, _old_pass, _newpass, _token);
        //                    }
        //                    catch (Exception objEx)
        //                    {
        //                        return Json(new { content = objEx, iserror = false }, JsonRequestBehavior.AllowGet);
        //                    }
        //                    if (isUpdate)
        //                    {
        //                        this.LogOut();
        //                        return Json(new { content = "Thay đổi mật khẩu thành công!\nVui lòng đăng nhập lại", iserror = isUpdate }, JsonRequestBehavior.AllowGet);
        //                    }
        //                    else
        //                    {
        //                        return Json(new { content = "Thay đổi mật khẩu không thành công!\nVui lòng thử lại", iserror = isUpdate }, JsonRequestBehavior.AllowGet);
        //                    }


        //                }
        //            }
        //            else
        //            {
        //                return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);
        //            }
        //            #endregion
        //        }
        //        else return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);

        //    }
        //    else
        //    {
        //        return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);
        //    }
            
        //}
        #endregion
        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LogOff()
        //{
        //    //HttpCookie cookie = new HttpCookie("Cookie1", "");
        //    //cookie.Expires = DateTime.Now.AddYears(-1);
        //    //Response.Cookies.Add(cookie);

        //    //FormsAuthentication.SignOut();
        //    //return RedirectToAction("Login", "Account", null);

        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        //    return RedirectToAction("Index", "Home");
        //}

        //[HttpPost]
        //[Authorize]
      //  [ValidateAntiForgeryToken]
        public ActionResult LogOut()
        {
            try
            {
                IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;
                authenticationManager.SignOut();
                Session.Abandon();

                // Clear authentication cookie
                HttpCookie rFormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                rFormsCookie.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(rFormsCookie);

                // Clear session cookie 
                HttpCookie rSessionCookie = new HttpCookie("ASP.NET_SessionId", "");
                rSessionCookie.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(rSessionCookie);
                //
                System.Web.HttpContext.Current.Session["access_token"] = string.Empty;
                System.Web.HttpContext.Current.Session["expires"] = string.Empty;
                System.Web.HttpContext.Current.Session["token_id"] = string.Empty;
                System.Web.HttpContext.Current.Session["unique_id"] = string.Empty;
                System.Web.HttpContext.Current.Session["isrequire"] = string.Empty;
                // Invalidate the Cache on the Client Side
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetNoServerCaching();
                Response.Cache.SetNoStore();
                Session.Clear();
                FormsAuthentication.SignOut();

                return RedirectToAction("Login", "Account");
            }
            catch
            {
                return RedirectToAction("Login", "Account");

            }
        }
        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        #region Private Process
        /// <summary>
        /// Đăng nhập hệ thống
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private Task<bool> LoginSystemAPI(string username, string password,ref UserProfile userProfile,ref string token_id,ref string access_token)
        {
            string URL_MASTER = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["HttpBaseAddress"]);
            string api_name = "api/Account/Login";
            string _mmyy = string.Format("{0:MMyy}", DateTime.Now);
            string _deserialize = string.Empty;
            string _message = string.Empty;
            #region Login
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // HTTP POST
                var login_model = new { username = username, password = PublicSecureProvider.Encrypt(password), mmyy = _mmyy };
                string json_data = JToken.FromObject(login_model).ToString();
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");

                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                try
                {
                    HttpResponseMessage response = client.PostAsync(_Uri, content).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        var _token = JsonConvert.DeserializeObject<Token>(result);
                        _deserialize = Base64Serializer.Base64Decode(_token.profile);
                        token_id = _token.token_id;
                        access_token = _token.access_token;
                        userProfile = JsonConvert.DeserializeObject<UserProfile>(_deserialize);

                        System.Web.HttpContext.Current.Session["access_token"] = _token.access_token;
                        System.Web.HttpContext.Current.Session["expires"] = _token.expires;
                        System.Web.HttpContext.Current.Session["token_id"] = _token.token_id;
                        System.Web.HttpContext.Current.Session["unique_id"] = PublicSecureProvider.Encrypt(userProfile.userid);
                        System.Web.HttpContext.Current.Session["isrequire"] = userProfile.RequireChangePassword;
                        return Task.FromResult(true);
                    }
                    else
                    {
                        userProfile = null;
                        token_id =null;
                        access_token = null;
                        return Task.FromResult(false);
                    }
                }
                catch (Exception ex)
                {
                    userProfile = null;
                    token_id = null;
                    access_token = null;
                    return Task.FromResult(false);
                }
            }
            #endregion

        }

       
        #endregion
    }
}