﻿using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Serilog;

namespace TAS.WebPortal.Filters
{
    /// <summary>
    /// 
    /// </summary>
    public class ApplicationAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
#if DEBUG
            if (IsSwaggerRequest(context: actionContext))
            {
                return true;
            }
#endif

            var requestScope = actionContext.Request.GetDependencyScope();
            //var rolePermissionService = requestScope.GetService(typeof(IRolePermissionService)) as IRolePermissionService;
            //var userService = requestScope.GetService(typeof(IUserService)) as IUserService;
            //var userContext = requestScope.GetService(typeof(IUserContext)) as IUserContext;
            //  var dataAccess = requestScope.GetService(typeof(IDataAccess)) as IDataAccess;
            var logger = requestScope.GetService(typeof(ILogger)) as ILogger;
            var principal = actionContext.RequestContext.Principal;
            var result = base.IsAuthorized(actionContext);
            var userId = principal.Identity.GetUserId();
            try
            {
                logger.Information("Result from authorization of .NET {@result}", result);
                if (result)
                {
                    //result = rolePermissionService.IsAuthorized(
                    //	principal.Identity.GetUserId(),
                    //	actionContext.ControllerContext.ControllerDescriptor.ControllerName,
                    //	actionContext.Request.Method,
                    //	actionContext.Request.RequestUri.ToString()
                    //);
                    logger.Information("Result from authorization of .NET {@result} with {@user}", result, userId);

                    //userContext.CurrentUser = userService.Get(userId);
                    //HttpContext.Current.Items.Add(nameof(UserContext), userContext);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex,
                    "Error while authorizing the api {@controller} with method {@method} at {@url}",
                    actionContext.ControllerContext.ControllerDescriptor.ControllerName,
                    actionContext.Request.Method,
                    actionContext.Request.RequestUri.ToString());
            }



            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        protected virtual bool IsSwaggerRequest(HttpActionContext context)
        {
            var request = context.Request;
            return request.RequestUri.AbsolutePath.Contains("swagger/ui/index");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task OnAuthorizationAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            return base.OnAuthorizationAsync(actionContext, cancellationToken);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            base.HandleUnauthorizedRequest(actionContext);
        }
    }
}