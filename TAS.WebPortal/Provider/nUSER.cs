﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using TAS.Core.Shared;
using TAS.WebPortal.Models;

namespace TAS.WebPortal.Provider
{
    public class nUSER
    {
        private string _Url = ConfigurationManager.AppSettings["HttpBaseAddress"];
        private string _token = string.Empty;

        public nUSER()
        {

        }
        public nUSER(string _token)
        {
            this._token = _token;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool ResetMatkhau(string obj)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                string api_name = string.Format("api/Survey/ResetMatkhau");
                InputModel objA = new InputModel();
                List<KeyValuePair<string, object>> lst = new List<KeyValuePair<string, object>>();
                lst.Add(new KeyValuePair<string, object>("id", obj));
                objA.ParaMeter = lst;
                string json_data = JToken.FromObject(objA).ToString();
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.PostAsync(_Uri, content).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool Insert_HR_Employee(Employee obj)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                string api_name = string.Format("api/Survey/Insert_HR_Employee");
                string json_data = JToken.FromObject(obj).ToString();
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.PostAsync(_Uri, content).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }

        public bool Update_HR_Employee(Employee obj)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                string api_name = string.Format("api/Survey/Update_HR_Employee");
                string json_data = JToken.FromObject(obj).ToString();
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.PostAsync(_Uri, content).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool Check_UserExist(string obj)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                string api_name = string.Format("api/Survey/Check_UserExist?obj={0}",obj);
              
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.GetAsync(_Uri).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    { bool bok=objTmp.ResultData.ToString().ToLower().Equals("true");
                        return bok;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }
    }
}