﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Text;
using System.Web;
using TAS.Core.Shared;
using TAS.Core.Shared.nCoV;
using TAS.WebPortal.Models;
namespace TAS.WebPortal.Provider
{
    public class nCOV
    {
        private string _Url = ConfigurationManager.AppSettings["HttpBaseAddress"];
        private string _token = string.Empty;
        private string _branchid = string.Empty;
        public nCOV()
        {

        }
        public nCOV(string _token)
        {
            this._token = _token;
        }

        public nCOV(string _token, string branchid)
        {
            this._token = _token;
            _branchid = branchid;
        }
        /// <summary>
        /// Lưu thông tin sức khỏe
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Save(ToKhai obj)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                string api_name = string.Format("api/Survey/UpSurveyForm");
                string json_data = JToken.FromObject(obj).ToString();
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.PostAsync(_Uri, content).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }
        /// <summary>
        /// Lưu thông tin sức khỏe
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public long Save_Person(TokhaiPerson obj)
        {
            using (HttpClient client = new HttpClient())
            {
                long iReturn = -1;
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                string api_name = string.Format("api/Person/UpSurveyForm");
                string json_data = JToken.FromObject(obj).ToString();
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.PostAsync(_Uri, content).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        iReturn =long.Parse(JsonConvert.DeserializeObject<string>(objTmp.ResultData.ToString()));
                    }
                    else
                    {
                        iReturn = -1;
                    }
                }
                return iReturn;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool Update_HR_Employee (HR_Employees obj)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                string api_name = string.Format("api/Survey/Update_HR_Employee");
                string json_data = JToken.FromObject(obj).ToString();
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.PostAsync(_Uri, content).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }

        #region Report


        /// <summary>
        /// Lấy danh sách tổng kết của các phòng khoa
        /// </summary>
        /// <param name="ngay">Ngày</param>
        /// <param name="maKp">-1 là lấy tất cả</param>
        /// <returns></returns>
        public DataSet GetReportDaily(string ngay, int maKp = -1,int loai=-1)
        {
            using (HttpClient client = new HttpClient())
            {
                DataSet ds = new DataSet();
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                ///_ngay, _gio, _makp
                string api_name = string.Format("api/Department/GetReportDaily?_makp={0}&ngay={1}&loai={2}", maKp, ngay,loai);
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.GetAsync(_Uri).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        ds = JsonConvert.DeserializeObject<DataSet>(objTmp.ResultData.ToString());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(objTmp.Message))
                        {
                            ds= null;
                        }
                    }
                }
                else
                {
                    ds = null;
                }
                return ds;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ngay"></param>
        /// <param name="maKp"></param>
        /// <returns></returns>
        public DataSet GetReportDaily_Detail(string ngay, int maKp = -1)
        {
            using (HttpClient client = new HttpClient())
            {
                DataSet ds = new DataSet();
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                ///_ngay, _gio, _makp
                string api_name = string.Format("api/Department/GetReportDailyDetails?_makp={0}&ngay={1}", maKp, ngay);
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.GetAsync(_Uri).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        ds = JsonConvert.DeserializeObject<DataSet>(objTmp.ResultData.ToString());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(objTmp.Message))
                        {
                            ds = null;
                        }
                    }
                }
                else
                {
                    ds = null;
                }
                return ds;
            }
        }


        public HR_Employees_Extend GetHR_Employees(string employeeId)
        {
            if (!string.IsNullOrEmpty(employeeId))
            {
                using (HttpClient client = new HttpClient())
                {
                    HR_Employees_Extend objReturn = new HR_Employees_Extend();
                    client.BaseAddress = new Uri(_Url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                    string api_name = string.Format("api/Survey/GetEmployeeInfo");
                    var objKiemtra = new InputModel();
                    var ParaMeter = new List<KeyValuePair<string, object>>();
                    ParaMeter.Add(new KeyValuePair<string, object>("empcode", employeeId));
                    objKiemtra.ParaMeter = ParaMeter;
                    //
                    //string json_data = JToken.FromObject(objKiemtra).ToString();
                    string json_data = JToken.FromObject(objKiemtra).ToString(Newtonsoft.Json.Formatting.None);
                    var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                    Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                    HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                    try
                    {
                        response = client.PostAsync(_Uri, content).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                            if (objTmp.ResultCode == 0)
                            {
                                objReturn = JsonConvert.DeserializeObject<HR_Employees_Extend>(objTmp.ResultData.ToString());
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objTmp.Message))
                                {
                                    objReturn = null;
                                }
                            }
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                        {
                            objReturn = null;
                        }
                        else
                        {
                            objReturn = null;

                        }
                    }
                    catch (Exception ex)
                    {
                        objReturn = null;
                    }

                    return objReturn;
                }
            }
            else
            {
                HR_Employees_Extend objReturn = new HR_Employees_Extend();
                return objReturn;
            }
        }


        public List<HR_Department> GetKhoaphong(string branchid)
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<HR_Department> objReturn = new List<HR_Department>();
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/Survey/GetKhoaphong");
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<HR_Department>>(objTmp.ResultData.ToString());
                            if(!string.IsNullOrEmpty(branchid))
                                objReturn = objReturn.Where(h => h.BranchID == branchid).ToList();
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion
        }
        #endregion

        #region Report by Branch
        /// <summary>
        /// Lấy danh sách tổng kết của các phòng khoa
        /// </summary>
        /// <param name="ngay">Ngày</param>
        /// <param name="maKp">-1 là lấy tất cả</param>
        /// <returns></returns>
        public DataSet GetRptBranchDaily(string ngay,string branchid, int maKp = -1)
        {
            using (HttpClient client = new HttpClient())
            {
                DataSet ds = new DataSet();
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                ///_ngay, _gio,_branchid, _makp
                var objKiemtra = new InputModel();
                var ParaMeter = new List<KeyValuePair<string, object>>();
                ParaMeter.Add(new KeyValuePair<string, object>("_ngay", ngay));
                ParaMeter.Add(new KeyValuePair<string, object>("_gio", "08:00"));
                ParaMeter.Add(new KeyValuePair<string, object>("_branchid", branchid));
                ParaMeter.Add(new KeyValuePair<string, object>("_makp", maKp));
                ParaMeter.Add(new KeyValuePair<string, object>("_loai", -1));
                objKiemtra.ParaMeter = ParaMeter;
                string json_data = JToken.FromObject(objKiemtra).ToString(Newtonsoft.Json.Formatting.None);
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                string api_name = string.Format("api/Survey/GetRptBranch");
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                try
                {
                    response = client.PostAsync(_Uri, content).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            ds = JsonConvert.DeserializeObject<DataSet>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                ds = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        ds = null;
                    }
                    else
                    {
                        ds = null;

                    }
                }
                catch (Exception ex)
                {
                    ds = null;
                }

                return ds;
 
            }
        }


        public List<Sys_Branch> GetList(string branchid ="")
        {
            List<Sys_Branch> lst = new List<Sys_Branch>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/Customer/GetBranchs?branchid={0}", branchid);
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.GetAsync(_Uri).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        lst = JsonConvert.DeserializeObject<List<Sys_Branch>>(objTmp.ResultData.ToString());
                    }
                }
            }
            return lst;
        }
        #endregion

        #region HR_Dept_Report

        public List<HR_Dept_Report> GetList_HR_Dept_Report()
        {
            using (HttpClient client = new HttpClient())
            {
                List<HR_Dept_Report> ds = new List<HR_Dept_Report>();
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                ///_ngay, _gio, _makp
                string api_name = string.Format("api/Department/Get_HR_dept_report");
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.GetAsync(_Uri).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        ds = JsonConvert.DeserializeObject<List<HR_Dept_Report>>(objTmp.ResultData.ToString());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(objTmp.Message))
                        {
                            ds = null;
                        }
                    }
                }
                else
                {
                    ds = null;
                }
                return ds;
            }
        }


        public bool Add_HR_Dept_Report(HR_Dept_Report obj)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
                string api_name = string.Format("api/Department/Add_HR_Dept_Report");
                string json_data = JToken.FromObject(obj).ToString();
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                Uri _Uri = new Uri(string.Format("{0}/{1}", _Url, api_name));
                HttpResponseMessage response = client.PostAsync(_Uri, content).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                    if (objTmp.ResultCode == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }

        #endregion
    }
}