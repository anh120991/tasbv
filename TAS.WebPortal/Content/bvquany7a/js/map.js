function initMap() {
	var myLatlng = new google.maps.LatLng(10.755756, 106.659105);
	var myOptions = {
	  zoom: 17,
	  center: myLatlng,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	var map = new google.maps.Map(document.getElementById("map"), myOptions);

	var contentString = '<div id="content">'+
		'<h1>Phòng khám Đột quỵ SIS</h1>'+
		'<div>'+
		"<p>53 Phạm Hữu Chí, P.12, Q.5, TPHCM</p>"+
		'</div>'+
		'</div>';

	var infowindow = new google.maps.InfoWindow({
		content: contentString,
		maxWidth: 700
	});

	var companyImage = new google.maps.MarkerImage(path+'pin.png',
		new google.maps.Size(51,69),
		new google.maps.Point(0,0)
	);

	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: companyImage,
		title: 'Phòng khám Đột quỵ SIS'
	});

	marker.addListener('click', function() {
	 infowindow.open(map, marker);
   });

}
google.maps.event.addDomListener(window, "load", initialize);
