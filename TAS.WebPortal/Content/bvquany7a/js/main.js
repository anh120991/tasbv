function respon() {
	var hfoot = $('.footer-container').outerHeight(),
		hhead = $('.header-container').outerHeight(),
		hscreen = $(window).outerHeight(),
		hbanner = hscreen - hhead;
	$('.page-wrap').css({marginBottom:-hfoot});
	$('.main').css({paddingBottom:hfoot});
	$('.bn-slide .col, .bn-inner-1 .col').css({paddingTop:hhead});

	$('.smoothscroll').on('click', function(event) {
        var target = $(this.getAttribute('href'));
        if( target.length ) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top -hhead+20
            }, 1000);
        }
    });

}

//$(window).scroll(function(){
//  if ($(window).scrollTop() >= 55) {
//    $('.gotop').css({opacity:1});
//	$('.header-container').addClass('fixhead');
//   }
//   else {
//    $('.gotop').css({opacity:0});
//	$('.header-container').removeClass('fixhead');
//   }
//});


$(document).ready(function(){

	respon();


	if (/android|Android|avantgo|blackberry|Mobile|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|iPad|iPod|BlackBerry|IEMobile|Opera Mini|iPhone|up\.browser|up\.link|webos|webOS|wos/i.test(navigator.userAgent)) {
        $('.bgfix').addClass('mbbg');
	}

	$(".menu li").find("ul").prev().addClass("fa fa-angle-down");
	$(".menu li").find("ul").parent().append("<span class='subarrow'></span>");

	$(".menu li" ).hover(
	  function() {
		$(this).addClass('hover');
	  }, function() {
		$(this).removeClass('hover');
	  }
	);

	$('.menu .subarrow').each(function(){
		var currentid = $(this).attr('href');
		$(this).click(function(e){
	        e.preventDefault();
			 $(this).prev().toggle();
			 $(this).toggleClass('open');
		});
	});

	$('.control-page').each(function(){
		var currentid = $(this).attr('href');
		$(this).click(function(e){
	        e.preventDefault();
			 $(this).toggleClass('active-burger');
			 $(currentid).toggleClass('open-sub');
			 $('body').toggleClass('open-page');
		});
	});



	/* bg appointment */
	$('.bg').each(function() {
		var imgUrl1 = $(this).find('.bgimg').attr('src');
		$(this).fixbg({ srcimg : imgUrl1});
    });

	$('.btn-search').each(function(){
		var currentid = $(this).attr('href');
		$(this).click(function(e){
	     e.preventDefault();
			 $(this).hide();
			 $(currentid).toggleClass('open');
		});
	});
		$('.show-info a').each(function(){
			$(this).click(function(e){
		     e.preventDefault();
				 $(this).find('.fa').toggleClass('fa-arrow-circle-down');
				 $(this).find('.fa').toggleClass('fa-arrow-circle-up');
			});
		});




		$('.btn-close').click(function(e){
			$('.btn-search').show();
			 $('#search').toggleClass('open');
		});

	$('.selectpicker').selectpicker({dropupAuto:false});

	$('.datepicker').datetimepicker({
		 format: 'DD/MM/YYYY'
	});


	$('.show-tooltip').tooltip();

	$(".fileinput").fileinput({
		browseIcon: "",
		browseLabel: "Choose File",
		showPreview: false,
		showRemove: false,
		showUpload: false
	});

	$('.fancybox')
		.fancybox({
		    padding: 0,
			helpers : {
				overlay : {
					locked: false
				}
			}
		});

	$('.fancybox-media')
		.fancybox({
			openEffect : 'none',
			closeEffect : 'none',
			prevEffect : 'none',
			nextEffect : 'none',
		    padding: 2,
			arrows : false,
			helpers : {
				media : {},
				buttons : {},
				overlay : {
					locked: false
				}
			}
		});

		$('.result__head').scrollToFixed({
        limit: $('.footer-container').offset().top - 300
    });

});


$(document).on('hidden.bs.modal', function (event) {
  if ($('.modal.in').length) {
	$('body').addClass('modal-open');
  }
});

$(window).load(function() {

	respon();

	$('.banner').css({ height: 'auto'});

	$('#slider').slick({
		arrows: false,
		autoplay: true,
		autoplaySpeed: 3000,
		dots: true,
		fade: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 2000
	});

	$('.eheight').each(function() {
		$(this).find('.ecol').matchHeight();
	});

});

$(window).resize(function() {
	respon();
});
