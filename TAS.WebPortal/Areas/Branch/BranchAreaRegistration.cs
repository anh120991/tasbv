﻿using System.Web.Mvc;

namespace TAS.WebPortal.Areas.Branch
{
    public class BranchAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Branch";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Branch_default",
                "Branch/{controller}/{action}/{id}",
               new { controller = "Home", action = "Index", id = UrlParameter.Optional }
               //,
               //new string[] { "TAS.WebPortal.Controllers" }
            );
            
            context.MapRoute(
             name: "chitiet",
             url: "Home/Details/{id}/{ngay}/{thang}/{nam}",
             defaults: new { controller = "Home", action = "Details", id = UrlParameter.Optional, ngay = UrlParameter.Optional, thang = UrlParameter.Optional, nam = UrlParameter.Optional }
          //   namespaces: new string[] { "TAS.WebPortal.Areas.Branch.Controllers" }
             );
        }
    }
}