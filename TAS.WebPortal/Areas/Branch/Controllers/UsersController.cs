﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TAS.Core.Entities.MasterData.Administrative;
using TAS.Core.Shared;
using TAS.Core.Shared.nCoV;
using TAS.WebPortal.Controllers;
using TAS.WebPortal.Helper;
using TAS.WebPortal.Models;
using TAS.WebPortal.Provider;

namespace TAS.WebPortal.Areas.Branch.Controllers
{
    public class UsersController : AdminBaseController
    {
        private readonly string URL_MASTER = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["HttpBaseAddress"]);
        private readonly string _Province = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultProvince"]);
        private readonly string _CustomerID = "46b26f68-18fd-11ea-9cef-33538cee559b";
        private readonly int _pageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PAGESIZE"]);
        // GET: Survey
        private string _token;
        private string _token_id;
        private string _employeeId;
        public UsersController() : base()
        {
            if (User != null && !string.IsNullOrEmpty(User.Access_token))
            {
                _token = User.Access_token;
                _token_id = User.Token_ID;
                _employeeId = User.UserID;
            }
            else RedirectToActionPermanent("Login", "Account");
        }
        // GET: Branch/Users
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        //[Route("Branch/Users")]
        //public ActionResult Index()
        //{
        //    ViewBag.BranchName = base._BranchName;
        //    return View();
        //}

        [Route("Index")]
        public ActionResult Index(int? page, string Keyword, string currentFilter)
        {
            int pageSize = int.Parse(ConfigurationManager.AppSettings["PAGESIZE"].ToString());

            ViewBag.BranchName = base._BranchName;
            if (Keyword != null)
                page = 1;
            else
                Keyword = currentFilter;
            ViewBag.CurrentFilter = Keyword;
            var obj = (from a in Get_ListEmployees().AsEnumerable()
                       select a);
            if (!string.IsNullOrEmpty(Keyword))
                obj = obj.Where(m => m.Dienthoai.ToUpper().Contains(Keyword.ToUpper()) ||
                m.Hoten.ToUpper().Contains(Keyword.ToUpper()));
            ViewBag.Model = obj;
            ViewBag.Makp = Gen_Khoa(base._BranchID, -1);
            int PageNumber = (page ?? 1);
            return View();
        }

        #region Get Danh Mục
        /// <summary>
        /// Danh sách nhân viên
        /// </summary>
        /// <returns></returns>
        private List<HR_Employees_Extend> Get_ListEmployees()
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<HR_Employees_Extend> objReturn = new List<HR_Employees_Extend>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/Survey/Get_ListEmployees?branch={0}", base._BranchID);
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<HR_Employees_Extend>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion
        }

        /// <summary>
        /// Năm sinh
        /// </summary>
        /// <returns></returns>
        private string genNamSinh(int? namsinh)
        {

            int year = DateTime.Now.Year;
            int min = year - 70;
            int max = year - 18;

            string _html = string.Empty;
            for (int i = max; i >= min; i--)
            {

                if (namsinh.HasValue && namsinh.Value == i)
                {
                    _html += string.Format("<option value='{0}' selected>{1}</option>", i, i);
                }
                else
                {
                    _html += string.Format("<option value='{0}'>{1}</option>", i, i);
                }

            }
            return _html;
        }
        /// <summary>
        /// Giới tính
        /// </summary>
        /// <returns></returns>
        private string genPhai(int? phai)
        {
            string _html = string.Empty;

            if (phai.HasValue)
            {
                if (phai.Value == 1)
                {
                    _html = string.Format("<option value='{0}' >{1}</option>", 0, "Nam");
                    _html += string.Format("<option value='{0}' selected>{1}</option>", 1, "Nữ");
                }
                else
                {
                    _html = string.Format("<option value='{0}' selected>{1}</option>", 0, "Nam");
                    _html += string.Format("<option value='{0}'>{1}</option>", 1, "Nữ");
                }
            }
            else
            {
                _html = string.Format("<option value='{0}' selected>{1}</option>", 0, "Nam");
                _html += string.Format("<option value='{0}'>{1}</option>", 1, "Nữ");
            }

            return _html;
        }
        /// <summary>
        /// Quốc tịch
        /// </summary>
        /// <returns></returns>
        private string genQuocTich(string name)
        {
            string _html = string.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                if (!name.Equals("VN"))
                {
                    _html = string.Format("<option value='{0}' >{1}</option>", "VN", "Việt Nam");
                    _html += string.Format("<option value='{0}' selected>{1}</option>", "khac", "Khác");
                }
                else
                {
                    _html = string.Format("<option value='{0}' selected>{1}</option>", "VN", "Việt Nam");
                    _html += string.Format("<option value='{0}'>{1}</option>", "khac", "Khác");
                }
            }
            else
            {
                _html = string.Format("<option value='{0}' selected>{1}</option>", "VN", "Việt Nam");
                _html += string.Format("<option value='{0}'>{1}</option>", "khac", "Khác");
            }


            return _html;

        }
        private string Gen_Khoa(int mapk = -1)
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "department");
            List<HR_Department> lstDept = GetKhoaphong();
            if (lstDept != null && lstDept.Count > 0)
            {
                foreach (HR_Department obj in lstDept)
                {
                    if (obj.Ma == mapk)
                    {
                        _html += string.Format("<option value='{0}' selected>{1}</option>", obj.Ma, obj.Ten);
                    }
                    else
                    {
                        _html += string.Format("<option value='{0}'>{1}</option>", obj.Ma, obj.Ten);
                    }
                }
            }
            // _html = option_system.ToString();
            return _html;
        }
        /// <summary>
        /// Danh sách khoa phòng
        /// </summary>
        /// <returns></returns>
        private List<HR_Department> GetKhoaphong()
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<HR_Department> objReturn = new List<HR_Department>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/Survey/GetKhoaphong");
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<HR_Department>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion
        }
        /// <summary>
        /// Gen combo tinh
        /// </summary>
        /// <returns></returns>
        private string genTinh(string maTinh = "701")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdtt");
            List<DIC_BTDTT> lstBtdtt;
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                lstBtdtt = Get_Btdtt();
                if (lstBtdtt != null && lstBtdtt.Count > 0)
                {
                    foreach (var item in lstBtdtt)
                    {
                        if (item.MATT == maTinh)
                        {
                            _html += string.Format("<option value='{0}' selected>{1}</option>", item.MATT, item.TENTT);
                        }
                        else
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MATT, item.TENTT);
                        }
                    }
                }
                // again in memCacher Cache
                MemoryCacheHelper.Add(_key, lstBtdtt, DateTimeOffset.UtcNow.AddDays(1));
            }
            else
            {
                lstBtdtt = (List<DIC_BTDTT>)option_system;
                if (lstBtdtt != null && lstBtdtt.Count > 0)
                {
                    foreach (var item in lstBtdtt)
                    {
                        if (item.MATT == maTinh)
                        {
                            _html += string.Format("<option value='{0}' selected>{1}</option>", item.MATT, item.TENTT);
                        }
                        else
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MATT, item.TENTT);
                        }
                    }
                }
            }
            return _html;
        }
        /// <summary>
        /// Danh sách tỉnh, thành phố
        /// </summary>
        /// <returns></returns>
        private List<DIC_BTDTT> Get_Btdtt()
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<DIC_BTDTT> objReturn = new List<DIC_BTDTT>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/BTDTT/Get?mavung=-1");
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<DIC_BTDTT>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion

        }
        /// <summary>
        /// Gen combo quận
        /// </summary>
        /// <param name="matt">Mã tỉnh</param>
        /// <returns></returns>
        private string genQuan(string maQuan = "-1", string matt = "701")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdquan");
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                var lstbtdQuanfull = Get_Btdquan("-1");
                // again in memCacher Cache
                MemoryCacheHelper.Add(_key, lstbtdQuanfull, DateTimeOffset.UtcNow.AddDays(1));

                //
                if (lstbtdQuanfull != null && lstbtdQuanfull.Count > 0)
                {
                    List<DIC_BTDQUAN> list = (List<DIC_BTDQUAN>)lstbtdQuanfull.Where(o => o.MATT.Equals(matt)).ToList();
                    if (list != null && list.Count != 0)
                    {
                        foreach (var item in list)
                        {
                            if (item.MAQU == maQuan)
                            {
                                _html += string.Format("<option value='{0}' selected>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                            else
                            {
                                _html += string.Format("<option value='{0}'>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                        }
                    }

                }
            }
            else
            {
                List<DIC_BTDQUAN> lstbtdQuanfull = (List<DIC_BTDQUAN>)option_system;
                if (lstbtdQuanfull != null && lstbtdQuanfull.Count > 0)
                {
                    List<DIC_BTDQUAN> list = (List<DIC_BTDQUAN>)lstbtdQuanfull.Where(o => o.MATT.Equals(matt)).ToList();
                    if (list != null && list.Count != 0)
                    {
                        foreach (var item in list)
                        {
                            if (item.MAQU == maQuan)
                            {
                                _html += string.Format("<option value='{0}' selected>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                            else
                            {
                                _html += string.Format("<option value='{0}'>{1}</option>", item.MAQU, item.TENQUAN);
                            }
                        }
                    }

                }
            }
            return _html;
        }
        public string genComboQuan(string matt = "701")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdquan");
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                var lstbtdQuanfull = Get_Btdquan("-1");
                // again in memCacher Cache
                MemoryCacheHelper.Add(_key, lstbtdQuanfull, DateTimeOffset.UtcNow.AddDays(1));

                //
                if (lstbtdQuanfull != null && lstbtdQuanfull.Count > 0)
                {
                    List<DIC_BTDQUAN> list = (List<DIC_BTDQUAN>)lstbtdQuanfull.Where(p => p.MATT.Equals(matt)).ToList();
                    if (list != null && list.Count != 0)
                    {
                        foreach (var item in list)
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MAQU, item.TENQUAN);
                        }
                    }

                }
            }
            else
            {
                List<DIC_BTDQUAN> lstbtdQuanfull = (List<DIC_BTDQUAN>)option_system;
                if (lstbtdQuanfull != null && lstbtdQuanfull.Count > 0)
                {
                    List<DIC_BTDQUAN> list = (List<DIC_BTDQUAN>)lstbtdQuanfull.Where(o => o.MATT.Equals(matt)).ToList();
                    if (list != null && list.Count != 0)
                    {
                        foreach (var item in list)
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MAQU, item.TENQUAN);
                        }
                    }

                }
            }
            return _html;
        }
        /// <summary>
        /// Danh sách quận, huyện
        /// </summary>
        /// <returns></returns>
        private List<DIC_BTDQUAN> Get_Btdquan(string matt = "701")
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<DIC_BTDQUAN> objReturn = new List<DIC_BTDQUAN>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/BTDQUAN/Get?matt={0}", matt);
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<DIC_BTDQUAN>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion

        }

        /// <summary>
        /// Gen combo xã
        /// </summary>
        /// <param name="maqu">Mã quận</param>
        /// <returns></returns>
        private string genXa(string maXa = "-1", string maqu = "-1")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdpxa");
            var option_system = MemoryCacheHelper.GetValue(_key);
            List<DIC_BTDPXA> lstBtdpxa;
            if (option_system == null)
            {

                lstBtdpxa = Get_Btdpxa("-1");
                MemoryCacheHelper.Add(_key, lstBtdpxa, DateTimeOffset.UtcNow.AddDays(1));
            }
            else
            {
                lstBtdpxa = (List<DIC_BTDPXA>)option_system;
            }
            if (lstBtdpxa != null && lstBtdpxa.Count > 0)
            {
                List<DIC_BTDPXA> lstDept = (List<DIC_BTDPXA>)lstBtdpxa.Where(o => o.MAQU.Equals(maqu)).ToList();
                if (lstDept != null && lstDept.Count > 0)
                {
                    foreach (var item in lstDept)
                    {
                        if (item.MAPXA.Equals(maXa))
                        { _html += string.Format("<option value='{0}' selected>{1}</option>", item.MAPXA, item.TENPXA); }
                        else
                        {
                            _html += string.Format("<option value='{0}'>{1}</option>", item.MAPXA, item.TENPXA);
                        }

                    }
                }
            }

            return _html;
        }
        public string genComboXa(string maqu = "-1")
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "btdpxa");
            var option_system = MemoryCacheHelper.GetValue(_key);
            List<DIC_BTDPXA> lstBtdpxa;
            if (option_system == null)
            {

                lstBtdpxa = Get_Btdpxa("-1");
                MemoryCacheHelper.Add(_key, lstBtdpxa, DateTimeOffset.UtcNow.AddDays(1));
            }
            else
            {
                lstBtdpxa = (List<DIC_BTDPXA>)option_system;
            }
            if (lstBtdpxa != null && lstBtdpxa.Count > 0)
            {
                List<DIC_BTDPXA> lstDept = (List<DIC_BTDPXA>)lstBtdpxa.Where(o => o.MAQU.Equals(maqu)).ToList();
                if (lstDept != null && lstDept.Count > 0)
                {
                    foreach (var item in lstDept)
                    {
                        _html += string.Format("<option value='{0}'>{1}</option>", item.MAPXA, item.TENPXA);
                    }
                }
            }

            return _html;
        }
        /// <summary>
        /// Danh sách phường xã
        /// </summary>
        /// <returns></returns>
        private List<DIC_BTDPXA> Get_Btdpxa(string maqu)
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                List<DIC_BTDPXA> objReturn = new List<DIC_BTDPXA>();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/BTDPXA/Get?maqu={0}", maqu);
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<List<DIC_BTDPXA>>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion

        }
        #endregion


        public ActionResult AddUser()
        {
            ViewBag.NamSinh = genNamSinh(int.Parse(DateTime.Now.ToString("yyyy")));
            ViewBag.Phai = genPhai(0);
            ViewBag.Quoctich = genQuocTich("VN");
            ViewBag.KhoaPhong = Gen_Khoa(-1);
            ViewBag.Tinh = genTinh("701");
            ViewBag.Quan = genQuan("-1", "701");
            ViewBag.Xa = genXa("-1", "-1");
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult AddUser(Employee e, FormCollection fc)
        {

            bool isUpdate = false;
            if (System.Web.HttpContext.Current.Session["expires"] != null)
            {
                #region Check session
                /*Kiểm tra token còn hạn ko?*/
                string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                if (!string.IsNullOrEmpty(_date))
                {
                    DateTime date = Convert.ToDateTime(_date);
                    if (date < DateTime.Now)
                    {
                        return Redirect(Url.Content("~/"));
                        //return Json(new { content = "Hết phiên đăng nhập", iserror = true }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
                        _employeeId = PublicSecureProvider.Decrypt(tmp);
                        //
                        _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
                        _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                        nUSER objManager = new nUSER(_token);
                        if (objManager.Check_UserExist(fc["txtSdt"].ToString()))
                        {
                            //ViewBag.Thongtin = "Số điện thoại đã có người sử dụng";
                            //ViewBag.Error = "";
                            //return RedirectToAction("ListUser","ManageUser");
                            //return Json(new { content = "Số điện thoại đã có người sử dụng!", iserror = false }, JsonRequestBehavior.AllowGet);
                            //    return Content("<html><head><script>alert('Số điện thoại đã có người sử dụng!'); window.top.location = 'AddUser' </script></head></html>");
                            return Json(new { content = "Số điện thoại đã có người sử dụng!", iserror = true }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            try
                            {
                                Employee obj = new Employee();
                                obj.hoten = fc["txtName"].ToString();
                                obj.cmnd = fc["txtCMND"].ToString();
                                obj.namsinh = fc["cboNamsinh"].ToString();
                                obj.phai = int.Parse(fc["cboGioTinh"].ToString());
                                obj.quoctich = fc["cboQuocTich"].ToString();
                                obj.dienthoai = fc["txtSdt"].ToString();
                                obj.email = fc["txtEmail"].ToString();
                                obj.makp = int.Parse(fc["cboKhoaPhong"].ToString());
                                obj.matt = fc["cboTinh"].ToString();
                                obj.maqu = fc["cboQuan"].ToString();
                                obj.mapxa = fc["cboXa"].ToString();
                                obj.sonha = fc["txtDiaChi"].ToString();
                                obj.CustomerID = _CustomerID;
                                obj.BranchID = base._BranchID;
                                obj.employeeID = fc["txtSdt"].ToString();
                                obj.GroupID = Convert.ToInt32(fc["cboGroupID"].ToString());

                                isUpdate = objManager.Insert_HR_Employee(obj);

                                if (isUpdate)
                                {
                                    //Thêm thành công
                                    //return PartialView("partialSuccess");
                                    return Json(new { content = "Thêm người dùng thành công!", iserror = false }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    //Thêm bị lỗi
                                    //ViewBag.strNoti = "Thêm người dùng không thành công!";
                                    //return PartialView("partialNoti");
                                    return Json(new { content = "Thêm người dùng không thành công!", iserror = true }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            catch (Exception objEx)
                            {
                                return Json(new { content = "Thêm người dùng không thành công!", iserror = true }, JsonRequestBehavior.AllowGet);
                            }
                        }

                    }
                }
                else
                {
                    //return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);
                    return Redirect(Url.Content("~/"));
                }
                #endregion
            }
            else
            {
                return Redirect(Url.Content("~/"));
                //return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateInput(false)]
        public ActionResult UpdateUser(string id)
        {
            ViewBag.NamSinh = genNamSinh(int.Parse(DateTime.Now.ToString("yyyy")));
            ViewBag.Phai = genPhai(0);
            ViewBag.Quoctich = genQuocTich("VN");
            ViewBag.KhoaPhong = Gen_Khoa(-1);
            ViewBag.Tinh = genTinh("701");
            ViewBag.Quan = genQuan("-1", "701");
            ViewBag.Xa = genXa("-1", "-1");

            var obj = (from a in Get_ListEmployees().AsEnumerable() select a).ToList();

            var objUser = obj.FirstOrDefault(h => h.Dienthoai == id);

            Employee objEmp = new Employee()
            {
                dienthoai = objUser.Dienthoai,
                hoten = objUser.Hoten,
                namsinh = objUser.Namsinh,
                phai = objUser.Phai,
                sonha = objUser.Sonha,
            };

            return View(objEmp);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUser(Employee e, FormCollection fc)
        {

            bool isUpdate = false;
            if (System.Web.HttpContext.Current.Session["expires"] != null)
            {
                #region Check session
                /*Kiểm tra token còn hạn ko?*/
                string _date = System.Web.HttpContext.Current.Session["expires"].ToString();
                if (!string.IsNullOrEmpty(_date))
                {
                    DateTime date = Convert.ToDateTime(_date);
                    if (date < DateTime.Now)
                    {
                        return Redirect(Url.Content("~/"));
                        //return Json(new { content = "Hết phiên đăng nhập", iserror = true }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
                        _employeeId = PublicSecureProvider.Decrypt(tmp);
                        //
                        _token_id = System.Web.HttpContext.Current.Session["token_id"].ToString();
                        _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                        nUSER objManager = new nUSER(_token);

                        try
                        {
                            Employee obj = new Employee();
                            obj.hoten = fc["txtName"].ToString();
                            obj.cmnd = fc["txtCMND"].ToString();
                            obj.namsinh = fc["cboNamsinh"].ToString();
                            obj.phai = int.Parse(fc["cboGioTinh"].ToString());
                            obj.quoctich = fc["cboQuocTich"].ToString();
                            obj.dienthoai = fc["txtSdt"].ToString();
                            obj.email = fc["txtEmail"].ToString();
                            obj.makp = int.Parse(fc["cboKhoaPhong"].ToString());
                            obj.matt = fc["cboTinh"].ToString();
                            obj.maqu = fc["cboQuan"].ToString();
                            obj.mapxa = fc["cboXa"].ToString();
                            obj.sonha = fc["txtDiaChi"].ToString();
                            obj.CustomerID = _CustomerID;
                            obj.BranchID = base._BranchID;
                            obj.employeeID = fc["txtSdt"].ToString();
                            obj.GroupID = Convert.ToInt32(fc["cboGroupID"].ToString());

                            isUpdate = objManager.Update_HR_Employee(obj);

                            if (isUpdate)
                            {
                                //Thêm thành công
                                //return PartialView("partialSuccess");
                                return Json(new { content = "Cập nhật thông tin người dùng thành công!", iserror = false }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                //Thêm bị lỗi
                                //ViewBag.strNoti = "Thêm người dùng không thành công!";
                                //return PartialView("partialNoti");
                                return Json(new { content = "Cập nhật thông tin người dùng không thành công!", iserror = true }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception objEx)
                        {
                            return Json(new { content = "Cập nhật thông tin người dùng không thành công!", iserror = true }, JsonRequestBehavior.AllowGet);
                        }

                    }
                }
                else
                {
                    //return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);
                    return Redirect(Url.Content("~/"));
                }
                #endregion
            }
            else
            {
                return Redirect(Url.Content("~/"));
                //return Json(new { content = "Hết phiên đăng nhập", iserror = false }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Tìm kiếm thông tin
        public JsonResult FindUser(int makp, string key)
        {
            try
            {
                string strData = string.Empty;
                var obj = (from a in Get_ListEmployees().AsEnumerable()
                           select a).ToList();
                if (makp != -1)
                {
                    if (!string.IsNullOrEmpty(key))
                    {
                        var list = obj.Where(h => h.Makp == makp && (h.Hoten.Trim().ToUpper().Contains(key.ToUpper()) || h.Dienthoai.Trim().Contains(key))).ToList();

                        strData = GenDataSearch(list);
                    }
                    else
                    {
                        var list = obj.Where(h => h.Makp == makp).ToList();

                        strData = GenDataSearch(list);
                    }
                }
                else
                {
                    //Lấy tất cả
                    if (!string.IsNullOrEmpty(key))
                    {
                        var list = obj.Where(h => h.Hoten.Trim().Contains(key) || h.Dienthoai.Trim().Contains(key)).ToList();

                        strData = GenDataSearch(list);
                    }
                    else
                    {
                        strData = GenDataSearch(obj.ToList());
                    }
                }
                return Json(new { content = strData, iserror = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objEx)
            {
                return Json(new { content = objEx, iserror = true }, JsonRequestBehavior.AllowGet);
            }
        }

        private string GenDataSearch(List<HR_Employees_Extend> lst)
        {
            string strData = string.Empty;

            for (int i = 0; i < lst.Count; i++)
            {
                strData += "<tr>";
                strData += string.Format("<td>{0}</td>", lst[i].Dienthoai);
                strData += string.Format("<td style='text-transform:uppercase'>{0}</td>", lst[i].Hoten);
                strData += string.Format("<td style='text-align:center'>{0}</td>", lst[i].Namsinh);
                strData += string.Format("<td>{0}</td>", lst[i].Sonha); //số nhà
                strData += string.Format("<td>{0}</td>", lst[i].Tenkp); //tên khoa phòng
                if (lst[i].Isactive == 1)
                {
                    strData += "<td style='text-align:center'><input type ='checkbox' id ='isactive' name ='isactive' checked /></td>"; //isactive
                }
                else
                {
                    strData += "<td style='text-align:center'><input type ='checkbox' id ='isactive' name ='isactive' /></td>"; //isactive
                }

                strData += "<td align='center'> " +
                    "<a class='btn btn-sm btn-info btn-update'>Sửa</a> " +
                    "<a class='btn btn-sm btn-info btn-confirm' style='color:yellow' onclick='ConfirmReset()'> Reset mật khẩu</a>" +
                    "</td>";
                strData += "<td align='center'><input name='ChkAr' id='ChkAr' type='checkbox' class='custom-control custom-checkbox'/></td>";

                strData += "</tr>";
            }

            return strData;
        }

        #endregion
        public JsonResult ResetPass(string id)
        {
            //ResetMatkhau
            #region ResetMatKhau
            using (HttpClient client = new HttpClient())
            {
                HR_Employees_Extend objReturn = new HR_Employees_Extend();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/Survey/ResetMatkhau");
                var objKiemtra = new InputModel();
                var ParaMeter = new List<KeyValuePair<string, object>>();
                ParaMeter.Add(new KeyValuePair<string, object>("sdt", id));
                objKiemtra.ParaMeter = ParaMeter;
                //
                //string json_data = JToken.FromObject(objKiemtra).ToString();
                string json_data = JToken.FromObject(objKiemtra).ToString(Newtonsoft.Json.Formatting.None);
                var content = new StringContent(json_data, Encoding.UTF8, "application/json");
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;//= client.PostAsync(_Uri, content).Result;
                try
                {
                    response = client.PostAsync(_Uri, content).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<HR_Employees_Extend>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch (Exception ex)
                {
                    objReturn = null;
                }
            }
            #endregion

            return new JsonResult();
        }

        private string Gen_Khoa(string branchid, int mapk)
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}", "department");
            List<HR_Department> lstDept = new List<HR_Department>();//= GetKhoaphong();
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                lstDept = new nCOV(_token, branchid).GetKhoaphong(branchid);
                // again in memCacher Cache
                MemoryCacheHelper.Add(_key, lstDept, DateTimeOffset.UtcNow.AddDays(3));
            }
            else
            {
                lstDept = (List<HR_Department>)option_system;
            }
            if (lstDept != null && lstDept.Count > 0)
            {
                #region Administrator
                _html = string.Format("<option value='{0}' selected>{1}</option>", -1, "Tất cả");
                foreach (HR_Department obj in lstDept)
                {
                    if (obj.Ma == mapk)
                    {
                        _html += string.Format("<option value='{0}' selected>{1}</option>", obj.Ma, obj.Ten);
                    }
                    else
                    {
                        _html += string.Format("<option value='{0}'>{1}</option>", obj.Ma, obj.Ten);
                    }
                }
                #endregion
            }
            // _html = option_system.ToString();
            return _html;
        }

        private string PhanTrang()
        {
            return "";
        }
    }
}