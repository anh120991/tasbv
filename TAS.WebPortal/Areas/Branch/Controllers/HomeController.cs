﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAS.Core.Shared;
using TAS.Core.Shared.nCoV;
using TAS.WebPortal.Controllers;
using TAS.WebPortal.Helper;
using TAS.WebPortal.Provider;

namespace TAS.WebPortal.Areas.Branch.Controllers
{
    public class HomeController : AdminBaseController
    {
        // GET: Branch/Home
        private string _token;
        private string _token_id;

        public ActionResult Index()
        {
            if (System.Web.HttpContext.Current.Session["unique_id"] == null)
            {
                return Redirect(Url.Content("~/"));
            }

            string strDate = DateTime.Now.ToString("yyyy-MM-dd");
            int tongQuanSo = 0;
            int tongBaoCao = 0;
            int tongChuaBaoCao = 0;
            int tongBatThuong = 0;

            var tmp = System.Web.HttpContext.Current.Session["unique_id"].ToString();
            var _employeeId = PublicSecureProvider.Decrypt(tmp);

            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
            var employeeInfo = new nCOV(_token, base._BranchID).GetHR_Employees(_employeeId);
            #region group 1 quản trị
            if (base._GroupID == 1)
            {
                var lstBranch = new nCOV(_token).GetList();

                //Load combo Branch
                ViewBag.BranchName = GenBranch(lstBranch, base._BranchID);
                ViewBag.Type = 2;
            }
            else
            {
                ViewBag.BranchName = "<h4>" + base._BranchName + "</h4>";
                ViewBag.Type = 1;
            }
            #endregion
            string strData = genListKP(-1, ref tongQuanSo, ref tongBaoCao, ref tongChuaBaoCao, ref tongBatThuong);
            ViewBag.tongQuanSo = tongQuanSo;
            ViewBag.tongBaoCao = tongBaoCao;
            ViewBag.tongChuaBaoCao = tongChuaBaoCao;
            ViewBag.tongBatThuong = tongBatThuong;

            ViewBag.List = strData;
            ViewBag.Makp = Gen_Khoa(base._BranchID, -1);
            ViewBag.Date = strDate;

           

            return View();

        }
        #region Private

        private string GenBranch(List<Sys_Branch> lst, string branchID = "")
        {
            string strData = string.Empty;
            //strData += "<div class='row'>";
            strData += "<label class='col-md-2'>Khối cơ quan</label>";
            strData += "<select id='cboBranchID' class='col-md-3 form-control' onchange='cboBranchIDChange()'>";
            strData += "<option value='' selected >Tất cả</option>";
            foreach (var item in lst)
            {
                if (item.BranchID == branchID)
                {
                    strData += string.Format("<option value='{0}' selected >{1}</option>", item.BranchID, item.BranchName);
                }
                else
                {
                    strData += string.Format("<option value='{0}' >{1}</option>", item.BranchID, item.BranchName);
                }
            }
            strData += "</select>";
            //strData += "</div>";
            return strData;
        }

        public string genListKP(int mapk, ref int tongQuanSo, ref int tongBaoCao, ref int chuabaocao, ref int tongBatThuong)
        {
            string strData = string.Empty;
            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
            string ngay = DateTime.Now.ToString("dd/MM/yyyy");
            DataSet ds = new nCOV(_token, base._BranchID).GetRptBranchDaily(ngay, base._BranchID);
            int i = 1;

            if (ds != null && ds.Tables.Count > 0)
            {
                ///Gen dữ liệu
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    int _tongQuanSo = Convert.ToInt32(r["quanso"]);
                    int _tongBaoCao = Convert.ToInt32(r["soluong"]);
                    int _tongBatThuong = Convert.ToInt32(r["batthuong"]);
                    int _chuabaocao = _tongQuanSo - _tongBaoCao;

                    tongQuanSo += _tongQuanSo;
                    tongBaoCao += _tongBaoCao;
                    tongBatThuong += _tongBatThuong;
                    chuabaocao += _chuabaocao;
                    strData += string.Format("<tr data-makp='{6}' data-ngay='{7}'>" +
                                             "<td><center>{0}</center></td>" +
                                             "<td>{1}</td>" +
                                             "<td><center>{2}</center></td>" +
                                             "<td><center>{3}</center></td>" +
                                             "<td><center>{4}</center></td>" +
                                             "<td><center>{5}</center></td>" +
                                             "<td><center><a href='/Home/Details/{6}/{7}'><i class='fa fa-edit' title='Chi tiết'></i></a></center></td>" +
                                             " </tr>", i, r["tenkp"].ToString(), _tongQuanSo, _tongBaoCao, _chuabaocao, _tongBatThuong, r["makp"].ToString(), ngay);
                    i++;
                }
            }

            return strData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapk"></param>
        /// <param name="ngay"></param>
        /// <param name="BranchID"></param>
        /// <param name="type">Type = 2 khi chọn được branch</param>
        /// <returns></returns>
        public JsonResult genListKPByBranch(string ngay, string BranchID = "", int type = 1)
        {
            string strData = string.Empty;
            int tongQuanSo = 0;
            int tongBaoCao = 0;
            int chuabaocao = 0;
            int tongBatThuong = 0;
            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();

            DateTime dtNgay = Convert.ToDateTime(ngay);
            string strNgay = dtNgay.ToString("dd/MM/yyyy"); 

            //string ngay = DateTime.Now.ToString("dd/MM/yyyy");

            if (type == 1 && string.IsNullOrEmpty(BranchID))
            {
                BranchID = base._BranchID;
            }

            string strCombo = Gen_Khoa(BranchID, -1);

            DataSet ds = new nCOV(_token, BranchID).GetRptBranchDaily(strNgay, BranchID);
            int i = 1;

            if (!string.IsNullOrEmpty(BranchID))
            {

                if (ds != null && ds.Tables.Count > 0)
                {
                    ///Lấy một nhóm chi nhánh
                    foreach (DataRow r in ds.Tables[0].Rows)
                    {
                        //Lấy theo branch
                        int _tongQuanSo = Convert.ToInt32(r["quanso"]);
                        int _tongBaoCao = Convert.ToInt32(r["soluong"]);
                        int _tongBatThuong = Convert.ToInt32(r["batthuong"]);
                        int _chuabaocao = _tongQuanSo - _tongBaoCao;

                        tongQuanSo += _tongQuanSo;
                        tongBaoCao += _tongBaoCao;
                        tongBatThuong += _tongBatThuong;
                        chuabaocao += _chuabaocao;
                        strData += string.Format("<tr data-makp='{6}' data-ngay='{7}'>" +
                                                    "<td><center>{0}</center></td>" +
                                                    "<td>{1}</td>" +
                                                    "<td><center>{2}</center></td>" +
                                                    "<td><center>{3}</center></td>" +
                                                    "<td><center>{4}</center></td>" +
                                                    "<td><center>{5}</center></td>" +
                                                    "<td><center><a href='/Home/Details/{6}/{7}'><i class='fa fa-edit' title='Chi tiết'></i></a></center></td>" +
                                                    " </tr>", i, r["tenkp"].ToString(), _tongQuanSo, _tongBaoCao, _chuabaocao, _tongBatThuong, r["makp"].ToString(), strNgay);

                        i++;
                    }
                }
            }
            else
            {
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataView view = new DataView(ds.Tables[0]);
                    DataTable distinctValues = view.ToTable(true, "branchid", "branchname");

                    //Gom nhóm
                    foreach (DataRow r in distinctValues.Rows)
                    {
                        string _branchid = r["branchid"].ToString();
                        string _branchname = r["branchname"].ToString();
                        int QuanSo = 0;
                        int BaoCao = 0;
                        int BatThuong = 0;
                        int ChuaBaoCao = 0;
                        string strDataBranch = "<tbody class='hide'>";

                        DataRow[] results = ds.Tables[0].Select(string.Format("branchid = '{0}'", _branchid));

                        if(results !=  null && results.Count()> 0)
                        {
                            int j = 1;
                            foreach(DataRow row in results)
                            {
                                //Lấy theo branch
                                int _tongQuanSo = Convert.ToInt32(row["quanso"]);
                                int _tongBaoCao = Convert.ToInt32(row["soluong"]);
                                int _tongBatThuong = Convert.ToInt32(row["batthuong"]);
                                int _chuabaocao = _tongQuanSo - _tongBaoCao;

                                tongQuanSo += _tongQuanSo;
                                tongBaoCao += _tongBaoCao;
                                tongBatThuong += _tongBatThuong;
                                chuabaocao += _chuabaocao;

                                QuanSo += _tongQuanSo;
                                BaoCao += _tongBaoCao;
                                BatThuong += _tongBatThuong;
                                ChuaBaoCao += _chuabaocao;

                                //strDataBranch += string.Format("<tr class='content' data-makp='{6}' data-ngay='{7}'>" +
                                //                            "<td><center>{0}</center></td>" +
                                //                            "<td>{1}</td>" +
                                //                            "<td><center>{2}</center></td>" +
                                //                            "<td><center>{3}</center></td>" +
                                //                            "<td><center>{4}</center></td>" +
                                //                            "<td><center>{5}</center></td>" +
                                //                            "<td><center><a href='/Home/Details/{6}/{7}'><i class='fa fa-edit' title='Chi tiết'></i></a></center></td>" +
                                //                            " </tr>", j, row["tenkp"].ToString(), _tongQuanSo, _tongBaoCao, _chuabaocao, _tongBatThuong, row["makp"].ToString(), strNgay);
                                strDataBranch += string.Format("<tr>" +
                                                           "<td>{0}</td>" +
                                                           "<td>{1}</td>" +
                                                           "<td>{2}</td>" +
                                                           "<td>{3}</td>" +
                                                           "<td>{4}</td>" +
                                                           "<td>{5}</td>" +
                                                           "<td><a href='/Home/Details/{6}/{7}'><i class='fa fa-edit' title='Chi tiết'></i></a></td>" +
                                                           " </tr>", j, row["tenkp"].ToString(), _tongQuanSo, _tongBaoCao, _chuabaocao, _tongBatThuong, row["makp"].ToString(), strNgay);

                                j++;
                            }
                        }
                        //strData += string.Format("<tbody class='labels'><tr class='bold'>" +
                        //                            "<td><center><i class='fa fa-plus' aria-hidden='true'></i></center></td>" +
                        //                            "<td>{0}</td>" +
                        //                            "<td><center>{1}</center></td>" +
                        //                            "<td><center>{2}</center></td>" +
                        //                            "<td><center>{3}</center></td>" +
                        //                            "<td><center>{4}</center></td>" +
                        //                            "<td><center></center></td>" +
                        //                        " </tr></tbody>",_branchname, QuanSo, BaoCao, ChuaBaoCao, BatThuong);

                        strData += string.Format("<tbody class='labels'><tr>" +
                                                 "<td colspan='7'> "+
                                                 "<label for='{0}'>{1}</label>" +
                                                 "<input type='checkbox' name ='{0}' id = '{0}' data-toggle = 'toggle' /> " +
                                                 "</td> </tr></tbody>", _branchid, _branchname);

                        strDataBranch += "</tbody>";
                        strData += strDataBranch;
                    }
                }
            }

            return Json(new { content = strData, datacombo = strCombo, tongQuanSo = tongQuanSo, tongBaoCao = tongBaoCao, tongChuaBaoCao = chuabaocao, tongBatThuong = tongBatThuong, iserror = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getListAjax(int mapk, string ngay, string BranchID = "", int type = 1)
        {
            string strData = string.Empty;
            _token = System.Web.HttpContext.Current.Session["access_token"].ToString();

            DateTime dtNgay = Convert.ToDateTime(ngay);
            string strNgay = dtNgay.ToString("dd/MM/yyyy");
            if (type == 1) BranchID = base._BranchID;

            DataSet ds = new nCOV(_token, BranchID).GetRptBranchDaily(strNgay, BranchID, mapk); //GetReportDaily(strNgay, mapk);
            int i = 1;

            int tongQuanSo = 0;
            int tongBaoCao = 0;
            int tongChuaBaoCao = 0;
            int tongBatThuong = 0;

            if (ds != null && ds.Tables.Count > 0)
            {
                ///Gen dữ liệu
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    int _tongQuanSo = Convert.ToInt32(r["quanso"]);
                    int _tongBaoCao = Convert.ToInt32(r["soluong"]);
                    int _tongBatThuong = Convert.ToInt32(r["batthuong"]);
                    int _chuabaocao = _tongQuanSo - _tongBaoCao;

                    tongQuanSo += _tongQuanSo;
                    tongBaoCao += _tongBaoCao;
                    tongBatThuong += _tongBatThuong;
                    tongChuaBaoCao += _chuabaocao;
                    strData += string.Format("<tr data-makp='{6}' data-ngay='{7}'>" +
                                             "<td><center>{0}</center></td>" +
                                             "<td>{1}</td>" +
                                             "<td><center>{2}</center></td>" +
                                             "<td><center>{3}</center></td>" +
                                             "<td><center>{4}</center></td>" +
                                             "<td><center>{5}</center></td>" +
                                             "<td><center><a href='/Dashboard/Details/{6}/{7}'><i class='fa fa-edit' title='Chi tiết'></i></a></center></td>" +
                                             " </tr>", i, r["tenkp"].ToString(), _tongQuanSo, _tongBaoCao, _chuabaocao, _tongBatThuong, r["makp"].ToString(), strNgay);
                    i++;
                }

                return Json(new { content = strData, tongQuanSo = tongQuanSo, tongBaoCao = tongBaoCao, tongChuaBaoCao = tongChuaBaoCao, tongBatThuong = tongBatThuong }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { content = "" }, JsonRequestBehavior.AllowGet);
        }

        private string genTieuDe(DataTable dtbData)
        {
            string strData = string.Empty;

            if (dtbData != null && dtbData.Rows.Count > 0)
            {

                strData = "<tr>";
                strData += string.Format("<th> {0} </th>", "STT");
                strData += string.Format("<th> {0} </th>", "KHOA/ BAN");
                strData += string.Format("<th> {0} </th>", "HỌ VÀ TÊN");

                foreach (DataRow r in dtbData.Rows)
                {
                    strData += string.Format("<th> {0} </th>", r["short_text"].ToString().ToUpper());
                }

                strData += "</tr>";
            }

            return strData;
        }

        private string genNoiDung(DataTable dtbData)
        {
            string strData = string.Empty;

            if (dtbData != null && dtbData.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in dtbData.Rows)
                {
                    strData += "<tr>";
                    strData += string.Format("<td> {0} </td>", i);
                    strData += string.Format("<td> {0} </td>", r["tenkp"]);
                    strData += string.Format("<td> {0} </td>", r["hoten"]);
                    for (int j = 5; j < dtbData.Columns.Count; j++)
                    {

                        strData += string.Format("<td> <center> {0} </center> </td>", r[j]);
                    }
                    strData += "</tr>";

                    i++;
                }
            }
            else
            {
                strData = "<tr> <td colspan=20> <center>Không có dữ liệu</center>  </td> </tr>";
            }
            return strData;
        }

        private string Gen_Khoa(string branchid, int mapk)
        {
            string _html = string.Empty;
            string _key = string.Format("dic_{0}_{1}", "department", branchid);
            List<HR_Department> lstDept = new List<HR_Department>();//= GetKhoaphong();
            var option_system = MemoryCacheHelper.GetValue(_key);
            if (option_system == null)
            {
                _token = System.Web.HttpContext.Current.Session["access_token"].ToString();
                lstDept = new nCOV(_token, branchid).GetKhoaphong(branchid);
                // again in memCacher Cache
                MemoryCacheHelper.Add(_key, lstDept, DateTimeOffset.UtcNow.AddDays(3));
            }
            else
            {
                lstDept = (List<HR_Department>)option_system;
            }
            if (lstDept != null && lstDept.Count > 0)
            {
                #region Administrator
                _html = string.Format("<option value='{0}' selected>{1}</option>", -1, "Tất cả");
                foreach (HR_Department obj in lstDept)
                {
                    if (obj.Ma == mapk)
                    {
                        _html += string.Format("<option value='{0}' selected>{1}</option>", obj.Ma, obj.Ten);
                    }
                    else
                    {
                        _html += string.Format("<option value='{0}'>{1}</option>", obj.Ma, obj.Ten);
                    }
                }
                #endregion
            }
            // _html = option_system.ToString();
            return _html;
        }

        #endregion

        public ActionResult Details(int id, string ngay = "", string thang = "", string nam = "")
        {
            if (System.Web.HttpContext.Current.Session["unique_id"] == null)
            {
                return Redirect(Url.Content("~/"));
            }
            _token = string.Empty;
            string tieude = string.Empty;
            string noidung = string.Empty;
            DataSet ds = new nCOV(_token, null).GetReportDaily_Detail(ngay + "/" + thang + "/" + nam, id);
            if (ds != null && ds.Tables.Count > 0)
            {
                //Gen tiêu đề câu trả lời
                tieude = genTieuDe(ds.Tables[0]);

                if (ds.Tables.Count == 2)
                {
                    //Danh sách người dùng
                    noidung = genNoiDung(ds.Tables[1]);
                }
                else
                {
                    noidung = "<tr> <td colspan=20> <center>Không có dữ liệu</center> </td> </tr>";
                }
            }

            ViewBag.CauHoi = tieude;
            ViewBag.genNoiDung = noidung;

            return View();
        }

        // GET: Branch/Home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Branch/Home/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Branch/Home/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Branch/Home/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Branch/Home/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Branch/Home/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult test()
        {
            return View();
        }
    }
}
