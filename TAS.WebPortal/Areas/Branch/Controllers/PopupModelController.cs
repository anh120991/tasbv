﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using TAS.Core.Shared;
using TAS.WebPortal.Controllers;
using TAS.WebPortal.Models;

namespace TAS.WebPortal.Areas.Admin.Controllers
{
    public class PopupModelController : BaseController
    {
        private readonly string URL_MASTER = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["HttpBaseAddress"]);
        private readonly string _Province = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultProvince"]);
        private readonly int _pageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PAGESIZE"]);
        // GET: Survey
        private string _token;
        private string _token_id;

        public PopupModelController()
        {
            if (User != null && !string.IsNullOrEmpty(User.Access_token))
            {
                _token = User.Access_token;
                _token_id = User.Token_ID;
            }
        }


        // GET: Admin/PopupModel
        public ActionResult Index()
        {
            try
            {
                ViewBag.Model = AlertMessage().Tables[0].AsEnumerable().ToList(); 
            }
            catch 
            {

            }           
            return View();
        }
        /// <summary>
        /// Message
        /// </summary>
        /// <returns></returns>
        private DataSet AlertMessage()
        {
            if (string.IsNullOrEmpty(_token))
            {
                return null;
            }

            #region Get data
            using (HttpClient client = new HttpClient())
            {
                DataSet objReturn = new DataSet();
                client.BaseAddress = new Uri(URL_MASTER);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);

                string api_name = string.Format("api/Survey/AlertMessage");
                Uri _Uri = new Uri(string.Format("{0}/{1}", URL_MASTER, api_name));
                HttpResponseMessage response;
                try
                {
                    response = client.GetAsync(_Uri).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        APIResult objTmp = JsonConvert.DeserializeObject<APIResult>(result);
                        if (objTmp.ResultCode == 0)
                        {
                            objReturn = JsonConvert.DeserializeObject<DataSet>(objTmp.ResultData.ToString());
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objTmp.Message))
                            {
                                objReturn = null;
                            }
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        objReturn = null;
                    }
                    else
                    {
                        objReturn = null;

                    }
                }
                catch(Exception ex)
                {
                    objReturn = null;
                }

                return objReturn;
            }
            #endregion
        }
    }
}