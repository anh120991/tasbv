﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TAS.WebPortal
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            // name: "LogOut",
            // url: "Account/LogOut",
            // defaults: new { controller = "Account", action = "LogOut" },
            // namespaces: new string[] { "TAS.WebPortal.Controllers" }
            // );

            //routes.MapRoute(
            // name: "chitiet",
            // url: "Home/{action}/{id}/{ngay}/{thang}/{nam}",
            // defaults: new { controller = "Home", action = "Details", id = UrlParameter.Optional, ngay = UrlParameter.Optional, thang = UrlParameter.Optional, nam = UrlParameter.Optional },
            // namespaces: new string[] { "TAS.WebPortal.Areas.Branch.Controllers" }
         //);

            routes.MapRoute(
               name: "Homes",
               url: "trang-chu/{action}/{id}",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
               namespaces: new string[] { "TAS.WebPortal.Controllers" }
           );
            routes.MapRoute(
                name: "Users",
                url: "nguoi-dung/dang-nhap",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional },
                namespaces: new string[] { "TAS.WebPortal.Controllers" }
            );
            routes.MapRoute(
              name: "Survey",
              url: "nhan-vien/{action}/{id}",
              defaults: new { controller = "Survey", action = "Create", id = UrlParameter.Optional },
              namespaces: new string[] { "TAS.WebPortal.Controllers" }
          );
            routes.MapRoute(
               name: "ChangePass",
               url: "nguoi-dung/doi-mat-khau",
               defaults: new { controller = "Manage", action = "ChangePassword", id = UrlParameter.Optional },
               namespaces: new string[] { "TAS.WebPortal.Controllers" }
           );
            routes.MapRoute(
              name: "Dashboard",
              url: "bao-cao",
              defaults: new { controller = "Dashboard", action = "Index", id = UrlParameter.Optional },
              namespaces: new string[] { "TAS.WebPortal.Controllers" }
          );
            routes.MapRoute(
             name: "Person",
             url: "khai-bao",
             defaults: new { controller = "Person", action = "Index", id = UrlParameter.Optional },
             namespaces: new string[] { "TAS.WebPortal.Controllers" }
         );
            routes.MapRoute(
               name: "Default",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
               namespaces: new string[] { "TAS.WebPortal.Controllers" }
           );

        }
    }
}
