﻿
function NgayChange() {
    var makp = $('#cboMakp').val();
    var ngay = $('#txtNgay').val();
    $.ajax({
        type: 'POST',
        url: '/Home/getListAjax',
        data: {
            mapk: makp,
            ngay: ngay,
        }

    }).done(function (data) {
        if (data.content != '') {
            //Fill 
            $('#tbData').html(data.content);

            $('#tongQuanSo').html(data.tongQuanSo);
            $('#tongBaoCao').html(data.tongBaoCao);
            $('#tongChuaBaoCao').html(data.tongChuaBaoCao);
            $('#tongBatThuong').html(data.tongBatThuong);
        }
        else {
            var content = '<tr> <td colspan = 10> Không có dữ liệu </td> </tr>'

            $('#tbData').html(content);

            $('#tongQuanSo').html(0);
            $('#tongBaoCao').html(0);
            $('#tongChuaBaoCao').html(0);
            $('#tongBatThuong').html(0);
        }
    });
}

function cboMakp_Change() {
    var _branch = $('#cboBranchID').val();
    var makp = $('#cboMakp').val();
    var ngay = $('#txtNgay').val();
    var _type = $('#type').val();

//    var obj = "<option value='-2'>Chọn</option>";
    if (makp == '-2') {
        alert('Chọn khoa phòng cần xem!');
        return;
    }
    $.ajax({
        type: 'POST',
        url: '/Branch/Home/getListAjax',
        data: {
            mapk: makp,
            ngay: ngay,
            branch: _branch,
            type: _type,
        }

    }).done(function (data) {
        if (data.content != '') {
            //Fill 
            $('#tbData').html(data.content);

            $('#tongQuanSo').html(data.tongQuanSo);
            $('#tongBaoCao').html(data.tongBaoCao);
            $('#tongChuaBaoCao').html(data.tongChuaBaoCao);
            $('#tongBatThuong').html(data.tongBatThuong);
        }
        else {
            var content = '<tr> <td colspan = 10> Không có dữ liệu </td> </tr>'

            $('#tbData').html(content);

            $('#tongQuanSo').html(0);
            $('#tongBaoCao').html(0);
            $('#tongChuaBaoCao').html(0);
            $('#tongBatThuong').html(0);
        }
    });
}

function cboBranchIDChange() {
    //Combo chi nhánh thay đổi
    //Đổ dữ liệu cho phòng ban thuộc chi nhánh
    //Đổ dữ liệu lưới mới

    var _branch = $('#cboBranchID').val();
    var ngay = $('#txtNgay').val();

    //genListKPByBranch
    $.ajax({
        type: 'POST',
        url: '/Branch/Home/genListKPByBranch',
        data: {
            ngay: ngay,
            BranchID: _branch,
            type: 2,
        }

    }).done(function (data) {
        if (data.content != '') {
            //Fill 
            $('#tbData').html(data.content);

            $('#tongQuanSo').html(data.tongQuanSo);
            $('#tongBaoCao').html(data.tongBaoCao);
            $('#tongChuaBaoCao').html(data.tongChuaBaoCao);
            $('#tongBatThuong').html(data.tongBatThuong);

            //tongQuanSo = tongQuanSo, tongBaoCao = tongBaoCao, tongChuaBaoCao = chuabaocao, tongBatThuong = tongBatThuong

            //Combo khoa phòng
            $('#cboMakp').html(data.datacombo);

            $('[data-toggle="toggle"]').change(function () {
                $(this).parents().next('.hide').toggle();
            });

        }
        else {
            var content = '<tr> <td colspan = 10> Không có dữ liệu </td> </tr>'

            $('#tbData').html(content);

            $('#tongQuanSo').html(0);
            $('#tongBaoCao').html(0);
            $('#tongChuaBaoCao').html(0);
            $('#tongBatThuong').html(0);

            //Combo khoa phòng
            $('#cboMakp').html(data.datacombo);
        }
    });
}