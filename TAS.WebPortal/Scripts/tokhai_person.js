﻿
function InitControl() {
    var list = $('#form').find('> .form-group > table > tbody > tr > td > center > input[type=radio]');
    if (list != null) {
        for (var i = 0; i < list.length; i++) {
            if (document.getElementById(list[i]).value == '-1') {
                //set event click cho nó
                document.getElementById("myBtn").addEventListener("click", function () {
                    document.getElementsByName("txtKhac").re = "Hello World";
                });
            }
        }
    }
}
/**
 * Chỉ nhập số
 * @param {any} evt
 */
function keypress(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

/**
 * IN HOA
 * @param {any} evt
 */
function InHoa(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 96 && charCode < 123) {
        return false;
    }
    return true;
}

function Send() {
    if (!Validate())
        return;

    /*Dữ liệu người dùng*/
    var objperson = {
        personID: $('#txtSdt').val(),
        hoten: $('#txtName').val(),
        namsinh: $('#cboNamsinh').val(),
        phai: $('#cboGioTinh').val(),
        sonha: $('#txtDiaChi').val(),
        thon: $('#txtDiaChi').val(),
        cholam: '',
        matt: $('#cboTinh').val(),
        maqu: $('#cboQuan').val(),
        mapxa: $('#cboXa').val(),
        hotenkdau: '',
        dienthoai: $('#txtSdt').val(),
        email: $('#txtEmail').val(),
        cmnd: $('#txtCMND').val(),
        ngaycap: '',
        noicap: '',      
        quoctich: $('#cboQuocTich').val()
    };
    /*end*/

    var lstCauHoi = [];
    /*Danh sách câu hỏi trả lời*/
    //var list = $('#form').find('> .form-group > input.col-md-12')
    //length: 0prevObject: jQuery.fn.init [form#form]__proto__: Object(0)
    var list = $('#form').find('> .container .main .form-group > input.col-md-12');
    if (list != null) {
        for (var i = 1; i < list.length; i++) {
            //Bỏ qua trường hợp 0 là cái địa chỉ

            var ten = '#txtquestion' + i;

            var tr = $(list[i]);
            var tr = $(list[i]);
            var lstDapAn = [];
            var objDapAn = {
                questionID: parseInt(tr.data("questionid")),
                answerID: parseInt(tr.data("answerid")),
                answerType: 1,
                answerValue: -1, ///Giá trị
                answerOtherText: $(ten).val(), ///Giá trị
            };
            lstDapAn.push(objDapAn);

            var objCauHoi = {
                questionID: tr.data("questionid"),
                //questionText: tr.data("questiontext"),
                //questionOrder: tr.data("questionorder"),
                answers: lstDapAn
            }
            lstCauHoi.push(objCauHoi);
        }
    }
    /*end*/

    /*Danh sách câu hỏi chọn có ko??*/
    var listChon = $('#form').find('> .container .main .form-group > table > tbody > tr');
    if (listChon != null) {
        for (var i = 0; i < listChon.length; i++) {

            var tr = $(listChon[i]);
            var stt = parseInt(tr.data("answerid"));
            var ten = 'benh' + stt;
            var txtten = 'txtbenh' + stt;

            var lstDapAn = [];

            var objDapAn = {
                questionID: parseInt(tr.data("questionid")),
                answerID: parseInt(tr.data("answerid")),
                answerType: 0,
                answerValue: parseInt($('input[name=' + ten + ']:checked').val()), ///Giá trị
                answerOtherText: $('input[name=' + txtten + ']:checked').val(), ///Giá trị
            };
            lstDapAn.push(objDapAn);

            var objCauHoi = {
                questionID: tr.data("questionid"),
                //questionText: tr.data("questiontext"),
                //questionOrder: tr.data("questionorder"),
                answers: lstDapAn
            }

            lstCauHoi.push(objCauHoi);
        }
    }
    /*end*/

    var objToKhai = {
        survey_ID: '',
        surveyDate: '',
        person: objperson,
        get_Question: lstCauHoi,
        captcha: '',
    }

    var params = {
        objToKhai: JSON.stringify(objToKhai),
    };
    $.ajax({
        url: '/Person/Create',
        type: 'POST',
        data: params,
        dataType: "json",
        success: function (data) {
            if (!data.iserror) {
                //alert('Lỗi cập nhật thông tin!');

                $('#modalNoti').modal('toggle');
                $('#txtNoti').text('Lỗi cập nhật thông tin!');
            }
            else {
                //alert('Cập nhật thông tin thành công!');
                $('#modalSuccess').modal('toggle');
                $('#txtSuccess').text('Cập nhật thông tin thành công!');
            }
        },
    });

}

function Validate() {
    var ten = $('#txtName').val();
    var cmnd = $('#txtCMND').val();
    var sdt = $('#txtSdt').val();

    if (ten == '') {
        //alert('Chưa nhập tên!');
        $('#modalNoti').modal('toggle');
        $('#txtNoti').text('Chưa nhập tên!');
        return false;
    }

    //if (cmnd == '') {
    //    //alert('Chưa nhập chứng minh nhân dân!');
    //    $('#modalNoti').modal('toggle');
    //    $('#txtNoti').text('Chưa nhập chứng minh nhân dân!');
    //    return false;
    //}

    if (sdt == '') {
        //alert('Chưa nhập số điện thoại!');
        $('#modalNoti').modal('toggle');
        $('#txtNoti').text('Chưa nhập số điện thoại!');
        return false;
    }

    return true;
}

function cboTinh_Change() {
    var idTinh = $('#cboTinh').val();
    var obj = "<option value='-1'>Chọn</option>";

    $('#cboXa').html(obj);

    if (idTinh == '000') {
        $('#cboQuan').html(obj);
        return;
    }
    $.ajax({
        type: 'POST',
        url: '/Person/genComboQuan',
        data: {
            matt: idTinh,
        }

    }).done(function (data) {
        if (data != '') {

            $('#cboQuan').html(data);
        }
        else {

            $('#cboQuan').html(obj);
        }
    });
}

function cboQuan_Change() {
    var idQuan = $('#cboQuan').val();
    var obj = "<option value='-1'>Chọn</option>";
    if (idQuan == '-1') {
        $('#cboXa').html(obj);
        return;
    }

    $.ajax({
        type: 'POST',
        url: '/Person/genComboXa',
        data: {
            maqu: idQuan,
        }

    }).done(function (data) {
        if (data != '') {

            $('#cboXa').html(data);
        }
        else {

            $('#cboXa').html(obj);
        }
    });
}

 

function Edit() {
    /*Bật lại các control*/

    let txtName = document.getElementById('txtName');
    txtName.removeAttribute("disabled");

    let txtCMND = document.getElementById('txtCMND');
    txtCMND.removeAttribute("disabled");

    let cboNamsinh = document.getElementById('cboNamsinh');
    cboNamsinh.removeAttribute("disabled");

    let cboGioTinh = document.getElementById('cboGioTinh');
    cboGioTinh.removeAttribute("disabled");

    let cboQuocTich = document.getElementById('cboQuocTich');
    cboQuocTich.removeAttribute("disabled");

    let txtEmail = document.getElementById('txtEmail');
    txtEmail.removeAttribute("disabled");

    let cboKhoaPhong = document.getElementById('cboKhoaPhong');
    cboKhoaPhong.removeAttribute("disabled");

    let cboTinh = document.getElementById('cboTinh');
    cboTinh.removeAttribute("disabled");

    let cboQuan = document.getElementById('cboQuan');
    cboQuan.removeAttribute("disabled");

    let cboXa = document.getElementById('cboXa');
    cboXa.removeAttribute("disabled");

    let txtDiaChi = document.getElementById('txtDiaChi');
    txtDiaChi.removeAttribute("disabled");


    /*Bật nút cập nhật*/
    let btnUpdate = document.getElementById('btnUpdate');
    btnUpdate.removeAttribute("hidden");

    let btnEdit = document.getElementById('btnEdit');
    btnEdit.setAttribute("hidden", true);
}
