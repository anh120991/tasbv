﻿using Microsoft.Owin;
using Owin;
using System.Web;
using System.Web.UI;

[assembly: OwinStartupAttribute(typeof(TAS.WebPortal.Startup))]
namespace TAS.WebPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
          //   Page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            ConfigureAuth(app);
        }
    }
}
